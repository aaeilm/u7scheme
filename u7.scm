(use (srfi 13))

; xxx
;   grainy noise ?
;   glitching ?
;   use coords more often
;   image/texture pairs as more of a thing
;   make-texture with wrap, filter, etc settings

(change-directory "/home/mel/desktop/2 u7/prog")
(load-once bacterias
  (load-directory "content/bacterias"))
(load-once creatures
  (load-directory "content/creatures"))

(define good-colors
  (vector-map $f '#(
  ( 180 180  10)
  (  22  22  22)
  (  44  44  44)
  (  30 180 120)
  ( 180  40  40)
  (  80  60 180)
  )))

(define bodily
  (vector-map $f '#(
  ( 240  30   0)
  ( 150   0  80)
  ( 230  20  20)
  (  68  32  20)
  (  15  15  15)
  ; (  81  25   8)
  ; ( 200   0   0)
  )))

(define diatoms
  (vector-map $f '#(
  ( 160 160 170)
  ( 125 125 125)
  (  80 145  20)
  ( 120 145  20)
  )))

(define redblacks
  (vector-map $f '#(
  ( 200  45   0)
  (  10  10  10)
  (   5   5   5)
  )))

(define mold
  (vector-map $f '#(
  ( 125  60 125)
  (  60 200 125)
  (  60 125  60)
  ( 125 125 125)
  )))

(define C-white '(1 1 1))
(define C-black '(0 0 0))

;

#|
(define (mather args)
  (let* (
    (ct 50)
    (fn (or (args 'fn) (lambda (x) x)))
    (grid (make-vector ct))
    )
    (for 0 ct 1
      (lambda (i)
        (vector-set! grid i (fn (/ i ct)))
        ))
    (lambda ()
      (set-shader!)
      (set-color! 1 1 1)
      (for 0 ct 1
        (lambda (i)
          (rect!
            (* i 4)
            (- 300 (* (vector-ref grid i) 200))
            2
            2
            )))
      )))
(define m (mather (make-proto
  `(fn
    ,(lambda (x)
      (expt (abs (- x 0.5)) 1)
      ))
  )))
(define (frame)
  (m))
|#

; note: only actually works in the context of the next 2 fns
(define-syntax define-shader-param
  (ir-macro-transformer
    (lambda (expr inject compare)
      (let* (
        (symbol-replace
          (lambda (sym it with)
            (string->symbol
              (irregex-replace/all it (symbol->string sym) with))))
        (raw-name (inject (cadr expr)))
        (name (symbol-replace raw-name "_" "-"))
        (uniform-name (symbol->string raw-name))
        (uniform-val (format-id "~a-loc" (symbol-replace name "_" "-")))
        (fn-name (format-id "set-~a!" (symbol-replace name "_" "-")))
        (gl-func (caddr expr))
        (prog (inject 'prog))
        (args (inject 'args))
        )
        `(begin
          (define ,uniform-val (safe-get-uniform-location ,prog ,uniform-name))
          (define (,(inject fn-name) . to)
            (if (not (null? to))
                (when ,uniform-val
                  (apply ,gl-func ,uniform-val to))
                (let (
                  (val (,args ',name))
                  )
                  (when (and ,uniform-val val)
                    (if (pair? val)
                        (apply ,gl-func ,uniform-val val)
                        (,gl-func ,uniform-val val))))
                ))
          )
      )
      )))

(define +mode-alpha-mix+ 0)
(define +mode-mask+ 1)
(define +mode-add+ 2)
(define +mode-multiply+ 3)
(define +mode-difference+ 4)
(define +mode-exclude+ 5)

(define (mixer args)
  (let* (
    (prog (make-program
      (make-frag-string
        "
        uniform sampler2D tex_b;
        uniform int mode;

        #define MODE_ALPHA_MIX 0
        #define MODE_MASK 1
        #define MODE_ADD 2
        #define MODE_MULTIPLY 3
        #define MODE_DIFFERENCE 4
        #define MODE_EXCLUDE 5

        vec4 effect() {
          vec4 dst = texture2D(tex, uv_coord);
          vec4 src = texture2D(tex_b, uv_coord);

          vec3 cdst = dst.rgb;
          vec3 csrc = src.rgb;

          vec3 oc;
          float oa;

          switch (mode) {
            case MODE_MASK: {
              float val = max(max(cdst.r, cdst.g), cdst.b);
              oc = csrc;
              oa = val;
            } break;

            case MODE_EXCLUDE: {
              oc = abs(cdst-csrc*src.a);
              // oc = abs(cdst.rgb-csrc.bgr*src.a);
              oa = dst.a;
            } break;

            case MODE_MULTIPLY: {
              oc = cdst * csrc;
              oa = dst.a;
            } break;

            case MODE_ADD: {
              oc = cdst + csrc * src.a;
              oa = dst.a;
            } break;

            case MODE_DIFFERENCE: {
              oc = cdst - csrc * src.a;
              oa = dst.a;
            } break;

            case MODE_ALPHA_MIX:
            default: {
              oc = cdst * (1-src.a) + csrc * src.a;
              oa = dst.a * (1-src.a) + src.a;
            } break;
          }

          return vec4(oc.rgb, oa);
        }
        ")
      ))
    (fb-a (make-framebuffer window-width window-height))
    (fb-b (make-framebuffer window-width window-height))
    (tx-a (framebuffer.tex fb-a))
    (tx-b (framebuffer.tex fb-b))

    (tex-b-loc (safe-get-uniform-location prog "tex_b"))
    )

    (define-shader-param mode gl:uniform1i)

    (set-shader! prog)
    (when tex-b-loc
      (gl:uniform1i tex-b-loc 1))

    (lambda (msg)
      (cond
        ((eq? msg 'bind-a)
          (gl:bind-framebuffer gl:+framebuffer+ fb-a)
          (gl:clear gl:+color-buffer-bit+))
        ((eq? msg 'bind-b)
          (gl:bind-framebuffer gl:+framebuffer+ fb-b)
          (gl:clear gl:+color-buffer-bit+))
        ((eq? msg 'draw)
          (set-shader! prog)
          (set-uv-flip! #t)
          (set-mode!)

          (gl:active-texture gl:+texture1+)
          (gl:bind-texture gl:+texture-2d+ tx-b)

          (draw! tx-a)
          (set-uv-flip! #f)
          )
        ))
    ))

(define (adjustment args)
  (let* (
    (prog (make-program
      (make-frag-string
        "
        uniform vec3 brightness;
        uniform vec3 contrast;
        uniform vec3 hsl_adj;

        uniform int do_levels;
        uniform int do_hsl;

        uniform float blur_amt;
        uniform float sharpen_amt;

        uniform int invert;
        uniform int colorize;

        uniform float posterize_amt;

        const float k_sharpen[9] = float[](
          -1 , -1, -1 ,
          -1 ,  9, -1 ,
          -1 , -1, -1
        );

        const float sharpen_div = 1;

        const float k_blur[9] = float[](
          1 , 2 , 1 ,
          2 , 4 , 2 ,
          1 , 2 , 1
        );

        const float blur_div = 16;

        vec4 kernel(float amt, float kernel[9], float div) {
          vec2 offsets[9] = vec2[](
            vec2( -amt,  amt),
            vec2( 0.0f,  amt),
            vec2(  amt,  amt),
            vec2( -amt, 0.0f),
            vec2( 0.0f, 0.0f),
            vec2(  amt, 0.0f),
            vec2( -amt, -amt),
            vec2( 0.0f, -amt),
            vec2(  amt, -amt)
          );

          vec4 sample[9];
          for(int i = 0; i < 9; ++i) {
              sample[i] = texture2D(tex, uv_coord + offsets[i]);
          }

          vec4 col = vec4(0.0);
          for(int i = 0; i < 9; ++i) {
              col += sample[i] * kernel[i] / div;
          }

          return col;
        }

        // todo
        //   hsl before or after b&c?

        vec3 apply_contrast(vec3 num, vec3 amt) {
          return (num - 0.5) * amt + 0.5;
        }

        vec3 posterize(vec3 val, float amt) {
          return val - (fract(val * amt) / amt);
        }

        vec4 effect() {
          vec4 orig;

          if (blur_amt > 0) {
            orig = kernel(blur_amt, k_blur, blur_div);
          } else if (sharpen_amt > 0) {
            orig = kernel(sharpen_amt, k_sharpen, sharpen_div);
          } else {
            orig = texture2D(tex, uv_coord);
          }

          vec3 colors = orig.rgb;

          if (do_hsl > 0) {
            if (colorize == 0) {
              vec3 hsl_colors = RGBtoHSL(colors);
              hsl_colors.r = fract(hsl_colors.r + hsl_adj.r);
              hsl_colors.gb *= hsl_adj.gb;
              colors = HSLtoRGB(hsl_colors);
            } else {
              vec3 hsl_colors = RGBtoHSL(colors);
              hsl_colors.r = fract(hsl_adj.r);
              hsl_colors.gb *= hsl_adj.gb;
              colors = hsl(hsl_colors);
            }
          }

          if (do_levels > 0) {
            colors *= brightness;
            colors = apply_contrast(colors, contrast);
          }

          if (invert > 0) {
            colors = 1 - colors;
          }

          if (posterize_amt > 0) {
            colors = posterize(colors, posterize_amt);
          }

          return vec4(colors.rgb, orig.a);
        }
        ")
      ))
    (fb (make-framebuffer window-width window-height))
    (tex (framebuffer.tex fb))
    )
    (define-shader-param brightness gl:uniform3f)
    (define-shader-param contrast gl:uniform3f)
    (define-shader-param hsl_adj gl:uniform3f)

    (define-shader-param do_hsl gl:uniform1i)
    (define-shader-param do_levels gl:uniform1i)

    (define-shader-param invert gl:uniform1i)
    (define-shader-param colorize gl:uniform1i)

    (define-shader-param posterize_amt gl:uniform1f)
    (define-shader-param blur_amt gl:uniform1f)
    (define-shader-param sharpen_amt gl:uniform1f)

    (set-shader! prog)
      (set-brightness! 1 1 1)
      (set-contrast! 1 1 1)
      (set-hsl-adj! 0 1 1)

      (set-do-hsl! 0)
      (set-do-levels! 0)

      (set-invert! 0)
      (set-colorize! 0)

      (set-posterize-amt! 0)
      (set-blur-amt! 0)
      (set-sharpen-amt! 0)

    (lambda (msg)
      (cond
        ((eq? msg 'bind)
          (gl:bind-framebuffer gl:+framebuffer+ fb)
          (gl:clear gl:+color-buffer-bit+)
          )
        ((eq? msg 'draw)
          (set-shader! prog)
            (set-brightness!)
            (set-contrast!)
            (set-hsl-adj!)

            (set-do-hsl!)
            (set-do-levels!)

            (set-invert!)
            (set-colorize!)

            (set-posterize-amt!)
            (set-blur-amt!)
            (set-sharpen-amt!)

          (set-uv-flip! #t)
          (draw! tex)
          (set-uv-flip! #f)
          )
        )
      )
    ))

#|
(define adj (adjustment (make-proto
  '(invert 0)

  '(do-levels 1)
  '(brightness (1 0.6 1))
  '(contrast (1 5 1))

  '(do-hsl 1)
  '(hsl-adj (0.2 1.0 1))
  '(colorize 0)

  ; '(posterize-amt 2.2)

  '(blur-amt 0.009)
  ;'(sharpen-amt 0.001)
  )))

(define dsat (adjustment (make-proto
  '(saturation 0.)
  '(do-hsl 1)
  )))

(define ylo (make-texture (make-image "content/yellow.png")))
(define img2 (make-texture (make-image "content/videos/strep/000295.png")))

(define mx (mixer (make-proto
  '(mode 1)
  )))

(define t 0)

(define (frame)
  (+=! t 0.2)
  (set-shader!)
  (set-uv-flip! #f)

  (set-color! 0 0 1)
  (rect! 90 90 220 220)

  (mx 'bind-a)
  (set-color! 0.25)
  (rect! 100 100 200 200)
  (set-color! 1)
  (rect! 50 200 400 400)
  ; (draw! ylo -10 0)

  (adj 'bind)
  (set-color! 1 1 1)
  (draw! ylo 0 0)
  ; (draw! ylo (*+ (sin t) 5 -10) 0)
  ; (draw! img2)

  (mx 'bind-b)
  (adj 'draw)
  ; (draw! ylo 0 0)

  ; (dsat 'bind)
  ; (adj 'bind)
  ; (mx 'draw)

  (gl:bind-framebuffer gl:+framebuffer+ 0)
  ; (dsat 'draw)
  (mx 'draw)
  ; (adj 'draw)
  )

)

(define t 0)

(define (frame)
  (+=! t 0.04)

  (adj-settings 'hue (/ t 3))
  (adj 'bind)
  (set-shader!)
  (draw! ylo)
  (gl:bind-framebuffer gl:+framebuffer+ 0)
  (adj 'draw)
  )

)

|#

; todo params
(define (ripple)
  (let* (
    (prog (make-program
      (make-frag-string
        "
        uniform float time;
        vec4 effect() {
          vec2 scr = (gl_FragCoord.xy + vec2((480 - 640)/2, 0)) / vec2(480, 480);

          vec2 center = vec2(scr.x, 1-scr.y) - vec2(0.5, 0.5);

          float len = length(center);
          len = (sin(len * len * 50 - time * 4) / 2 + 0.5) * (1 - len);

          vec4 color = texture2D(tex, uv_coord + center*len/10);
          // color.r = len;
          return color;
        }
        ")
      ))
    (t 0)
    (tloc (safe-get-uniform-location prog "time"))
    (canv (make-framebuffer window-width window-height))
    (tex (framebuffer.tex canv))
    )
    (lambda (msg)
      (cond
        ((eq? msg 'update)
          (+=! t 0.04)
          )
        ((eq? msg 'bind)
          (gl:bind-framebuffer gl:+framebuffer+ canv)
          (gl:clear gl:+color-buffer-bit+)
          )
        ((eq? msg 'draw)
          (gl:bind-framebuffer gl:+framebuffer+ 0)
          (set-shader! prog)
          (when tloc
            (gl:uniform1f tloc t))
          (draw! tex 0 480 0 1 -1)
        ))
      )
    ))

; todo params or something
;   just like take a lambda to start wtih?
(define (reaction-diffusion)
  (let* (
    (rd-prog (make-program
      (make-frag-string
      "
      float dt = 0.04;

      /*
      float dA = 1.0;
      float dB = 0.6;
      float feed = 0.027;
      float kill = 0.053;
      */

      float dA = 1.0;
      float dB = 0.435;
      float feed = 0.0545;
      float kill = 0.062;

      /*
      float dA = 0.2;
      float dB = 1.3;
      float feed = 0.027;
      float kill = 0.092;
      */

      uniform float time;

      vec2 psize = 1./vec2(640,480);

      vec4 getcolor(sampler2D tx, vec2 pos, vec2 offset) {
        return texture2D(tx, pos + (offset * psize));
      }

      vec4 laplace(sampler2D tex, vec2 pos) {
        vec4 ret = vec4(0.);
        ret += -1 * getcolor(tex, pos, vec2(0., 0.));

        ret += 0.2 * getcolor(tex, pos, vec2(1., 0.));
        ret += 0.2 * getcolor(tex, pos, vec2(-1., 0.));
        ret += 0.2 * getcolor(tex, pos, vec2(0., 1.));
        ret += 0.2 * getcolor(tex, pos, vec2(0., -1.));

        ret += 0.05 * getcolor(tex, pos, vec2(1., -1.));
        ret += 0.05 * getcolor(tex, pos, vec2(1., 1.));
        ret += 0.05 * getcolor(tex, pos, vec2(-1., -1.));
        ret += 0.05 * getcolor(tex, pos, vec2(-1., 1.));

        return ret;
      }

      vec4 effect() {
        //                                              time    0.0001   0.0001
        //                                              time/3  0.00015  0.0001
        vec4 curr = texture2D(tex, uv_coord + (vec2(sin(time/15)*0.00015, 0.00008)));
        // curr = texture2D(tex, uv_coord);

        float a = curr.r;
        float b = curr.g;

        float t = dt;
        t = 1;
        a += t * (dA * laplace(tex, uv_coord).r
                  - a * b * b
                  + feed * (1 - a));

        b += t * (dB * laplace(tex, uv_coord).g
                  + a * b * b
                  - (kill + feed) * b);

        a = clamp(a, 0, 1);
        b = clamp(b, 0, 1);

        return vec4(a, b, 0, 1);
      }

      ")
      ))
    (draw-prog (make-program
      (make-frag-string
      "
      uniform float time;
      vec4 effect() {
        // vec4 c = texture2D(tex, uv_coord*2);
        vec4 c = texture2D(tex, uv_coord);
        // return vec4(c.g, abs(sin(time/8))*c.g, 0, smoothstep(0.0, 0.1, c.g));
        return vec4(c.g, 0, c.g, smoothstep(0.0, 0.1, c.g));
      }
      ")
      ))

    (fb-curr (make-framebuffer window-width window-height))
    (fb-last (make-framebuffer window-width window-height))
    (tex-curr (framebuffer.tex fb-curr))
    (tex-last (framebuffer.tex fb-last))
    (swap!
      (lambda ()
        (let (
          (temp fb-last)
          (temp-tx tex-last)
          )
          (set! fb-last fb-curr)
          (set! fb-curr temp)
          (set! tex-last tex-curr)
          (set! tex-curr temp-tx)
          )
        ))

    (t 0)
    (tloc (safe-get-uniform-location rd-prog "time"))
    (draw-tloc (safe-get-uniform-location draw-prog "time"))
    )
    (gl:bind-texture gl:+texture-2d+ tex-curr)
    (texture-set-wrap! gl:+texture-2d+ gl:+repeat+ gl:+repeat+)

    (gl:bind-texture gl:+texture-2d+ tex-last)
    (texture-set-wrap! gl:+texture-2d+ gl:+repeat+ gl:+repeat+)

    (gl:bind-framebuffer gl:+framebuffer+ fb-curr)
    (gl:clear gl:+color-buffer-bit+)

    (gl:bind-framebuffer gl:+framebuffer+ fb-last)
    (gl:clear gl:+color-buffer-bit+)

    #|

    (set-shader!)
    (set-color! 1 0 0)
    (rect! 0 0 640 480)

    (set-color! 1 1 0)
    ; (rect! 50 50 50 50)
    ; (circ! 150 60 40)
    ; (circ! 120 150 60)

    ; (print! "u7dratama" 100 200 0 8)
    (print! "sorry" 100 200 0 8)
    |#

    (gl:bind-framebuffer gl:+framebuffer+ 0)

    (lambda (msg)
      (cond
        ((eq? msg 'update)

      (for 0 25 1
      (lambda (i)

      (+=! t 0.04)
      (gl:bind-framebuffer gl:+framebuffer+ fb-curr)
      (gl:clear gl:+color-buffer-bit+)

      (set-shader! rd-prog)
      (when tloc
        (gl:uniform1f tloc t))
      (set-uv-flip! #t)
      ; (draw! tex-last 0 480 0 1 -1)
      (draw! tex-last)
      (set-uv-flip! #f)

      (gl:bind-framebuffer gl:+framebuffer+ 0)

      (swap!)
      )) ;for

          )
        ((eq? msg 'bind)
          (gl:bind-framebuffer gl:+framebuffer+ fb-last)
          (gl:clear gl:+color-buffer-bit+)
          )
        ((eq? msg 'draw)

      #|
      (set-shader!)
      (set-color! 1 1 1)
      (rect! 0 0 640 480)
      |#

      (set-shader! draw-prog)
      (when draw-tloc
        (gl:uniform1f draw-tloc t))
      (set-uv-flip! #t)
      (draw! tex-curr)
      (set-uv-flip! #f)
      )
        ))
    ))

#|
(define rd (reaction-diffusion))

(define (frame)
  (rd))

)
|#

(define (bar-chart args)
  (let* (
    (w     (or (args 'w) 25))
    (spc   (or (args 'spc) 10))
    (ct    (+ 1
             (or (args 'ct)
                 (inexact->exact (floor (/ window-width (+ w spc))))
                 )))
    (bmin  (or (args 'bmin) 10))
    (bmax  (or (args 'bmax) (- window-height 10)))
    (jmp   (or (args 'jmp) 34))
    (fspd  (or (args 'fspd) 3))
    (color (or (args 'color) '(1 1 1)))
    (F (make-framelimiter fspd))
    (P-move (make-rpg 0.4))
    (R-jump (make-rng (- jmp) (+ 1 jmp)))
    (heights (make-vec ct
               (lambda (i)
                 (rand bmin bmax))))
    )
    (lambda ()
      (when (F)
        (vector-map!
          (lambda (v)
            (if (P-move)
                (clamp (+ v (R-jump)) bmin bmax)
                v))
          heights))

      (apply set-color! color)
      (for 0 (- (len heights) 1) 1
        (lambda (i)
          (let (
            (y (vector-ref heights i))
            (x (+ spc (* i (+ w spc))))
            )
            (rect! x 0 w y)
            )
          ))
      )
    ))

#|
(define b (bar-chart (make-proto
  ; '(ct 10)
  ; '(fspd 2)
)))

(define (frame)
  (b))
|#

(define (bar-circles args)
  (let* (
    (r    (or (args 'r) 40))
    (fspd (or (args 'fspd) 3))
    (C    (or (args 'C) good-colors))
    (F (make-framelimiter fspd))
    (ct (floor (/+ (get-radius) r 2)))
    (circs (make-q ct
             (lambda (i)
               (rand-in C))))
    (cc (get-center))
    (cx (car cc))
    (cy (cdr cc))
    )
    (lambda ()
      (when (F)
        (queue.push! circs (rand-in C))
        (queue.pop! circs))
      (let loop (
        (lst (queue.as-list circs))
        (n 1)
        )
        (unless (null? lst)
          (apply set-color! (car lst))
          (circ! cx cy (* (- ct n) r))
          (loop (cdr lst) (+ n 1))
          ))
      )
    ))

#|
(define bc (bar-circles (make-proto
  )))

(define (frame)
  (bc))
|#

; todo progname
(define (snois args)
  (let* (
    (num (or (args 'num) 0))
    (prog0 (make-program
      (make-frag-string
      "
      uniform float time;

      vec4 effect() {
        float x = snoise(
          vec3(uv_coord.x * 3 + sin(uv_coord.y + time/2)
             , uv_coord.y * 3 + sin(uv_coord.x*10+time)/6
             , time))/2 + 0.5;
        float c = snoise(
          vec3(uv_coord.x * 3 + sin(uv_coord.y + time/2)
             , uv_coord.y * 3 + sin(uv_coord.x*7+time)/6 + sin(uv_coord.x*3-time)/2
             , time + 0.2))/2 + 0.5;
        c = smoothstep(0.2, 0.8, c);
        vec3 o = mix(vec3(0.4, 0.8, 0.3), vec3(0.4, 0.1, 0.2), x);
        vec3 r = mix(vec3(0.9, 1.0, 0.3), vec3(0.4, 0.8, 0.1), c);
        vec3 y = abs(r-o);
        // vec3 y = r*(1-o) + (1-r)*o;
        return vec4(y.rgb,1);
      }
      ")))
    (prog1 (make-program
      (make-frag-string
      "
      uniform float time;

      float posterize(float val, int amt) {
        return val - (fract(val * amt) / amt);
      }

      vec3 posterize(vec3 val, int amt) {
        return val - (fract(val * amt) / amt);
      }

      vec2 posterize(vec2 val, int amt) {
        return val - (fract(val * amt) / amt);
      }

      float p2(float val, int amt) {
        return val - sin((fract(val * amt) / amt) * PI/2);
      }

      vec4 effect() {
        float f = snoise(vec3(uv_coord.xy * 2.5, time))/2+0.5;
        // return vec4(cubehelix(f, sin(time), 0.9, 0.9, cos(time/3)*0.8+1).rgb, 1.0);
        f = 1-abs(f-0.75);

        // float x = posterize(f, 10);
        // f = x+p2(f, 10);

        vec3 col = hcy(vec3(f, mix(uv_coord.y, f, 1.), 1-f));

        // col.r = posterize(col.r, 4);
        // col.gb = posterize(col.gb, 10);

        return vec4(col, 1.0);
      }
      ")))
    (prog
      (cond
        ((= num 0) prog0)
        ((= num 1) prog1)
        (else prog0)))
    (tloc (safe-get-uniform-location prog "time"))
    (t 0)
    )
    (lambda ()
      (+=! t (get-fps))
      (set-shader! prog)
      (when tloc
        (gl:uniform1f tloc t))
      (draw-px! default-texture)
      )))

;; (define x (snois))

;; (define (frame)
  ;; (x))

;; )

(define (black-dots args)
  (let (
    (c (or (args 'c) '(0)))
    (sizes (or (args 'sizes) '(8 9)))
    (pts (gen-noise-pts 0.04 30 0.004 3 0.01))
    (F (make-framelimiter 2))
    )
    (vector-map
      (lambda (pt)
        (pt 'x (+ (pt 'x) (rand -20 21)))
        (pt 'y (+ (pt 'y) (rand -20 21)))
        (pt 'sz (apply rand sizes))
        )
      pts)
    (lambda ()
      (apply set-color! c)
      (vector-map
        (lambda (pt)
          (circ! (pt 'x) (pt 'y) (pt 'sz))
          (pt 'x (+ (pt 'x) (*+ (rand) 8 -4)))
          (pt 'y (+ (pt 'y) (*+ (rand) 8 -4)))
          )
        pts)
        )
    ))

#|
(define (vplayer video)
  (let* (
    (vid (make-anim video))
    (vimg (car (anim.peek vid)))
    (vw (image.w vimg))
    (vh (image.h vimg))
    )
    (lambda ()
      ; todo send in program and color?
      ; mayb not uset his at all
      (set-shader!)
      (set-color! 1 1 1)
      (draw! (cdr (anim.next! vid)) -10 -10 0 3 3)
      )
    ))
|#

; (define x (vplayer meat-vid))

; (define (frame)
  ; (x))

(define (mesher args)
  (assert
    (and
      (args 'obj-file)
      (args 'img-file)))
  (let* (
    (mesh
      (load-obj-file (args 'obj-file) gl:+stream-draw+))
    (original-coords
      (object-copy
        (mesh.verts mesh)))
    (tx
      (make-texture
        (make-image (args 'img-file))))
    (moves
      ; '((point-idxes) angle speed)
      (or (args 'moves) '()))
    (changed? (make-parameter #f))
    (draw-settings
      (or
        (args 'draw-settings)
        '(#t #f #f)))
    (draw-tex (car draw-settings))
    (draw-wire (cadr draw-settings))
    (draw-nums (caddr draw-settings))
    (cxy (get-center))
    (transform
      (apply make-coords
        (or
          (args 'transform)
          `(,(car cxy) ,(cdr cxy) 0 30 30)))
      )
    )

    (gl:bind-texture gl:+texture-2d+ tx)
    (texture-set-wrap! gl:+texture-2d+ gl:+clamp-to-border+ gl:+clamp-to-border+)
    (texture-set-border-color! gl:+texture-2d+ 0 0 0 0)
    (gl:bind-texture gl:+texture-2d+ 0)

    (lambda (msg)
      (cond
        ((number? msg)
          (let (
            (amt msg)
            (mesh-verts (mesh.verts mesh))
            )
            (map
              (lambda (move)
                (let* (
                  (pts (car move))
                  (a (cadr move))
                  (s (caddr move))
                  (oxy (to-cartesian (* s amt) a))
                  (ox (car oxy))
                  (oy (cdr oxy))
                  )
                  (map
                    (lambda (point)
                      (let (
                        (at-x (* point 4))
                        (at-y (*+ point 4 1))
                        )
                      (f32vector-set! mesh-verts at-x
                        (+ ox
                          (f32vector-ref original-coords at-x)))
                      (f32vector-set! mesh-verts at-y
                        (+ oy
                          (f32vector-ref original-coords at-y)))
                      ))
                    pts)
                  ))
              moves)
            )
          (changed? #t)
          )
        ((eq? msg 'draw)
          (when (changed?)
            ; (dnL 'ch)
            (mesh.push-changes! mesh 0 50)
            (changed? #f))

          (set-shader! default-shader)
          (set-color! 1 1 1)
          (apply set-transform-uniforms! (coords->list transform))

          (gl:bind-vertex-array (mesh.vao mesh))

          (when draw-wire
            (gl:polygon-mode gl:+front-and-back+ gl:+line+)
            (gl:active-texture gl:+texture0+)
            (gl:bind-texture gl:+texture-2d+ default-texture)
            (gl:draw-elements gl:+triangles+ (u32vector-length (mesh.indcs mesh)) gl:+unsigned-int+ (address->pointer 0))
            )

          (when draw-tex
            (gl:polygon-mode gl:+front-and-back+ gl:+fill+)
            (gl:active-texture gl:+texture0+)
            (gl:bind-texture gl:+texture-2d+ tx)
            (gl:draw-elements gl:+triangles+
              (u32vector-length (mesh.indcs mesh))
              gl:+unsigned-int+
              (address->pointer 0))
            )

          (when draw-nums
            ; todo make sure this is set for sprite shader
            (gl:active-texture gl:+texture0+)
            (for 0 (f32vector-length (mesh.verts mesh)) 4
              (lambda (i)
                (let (
                  (x (*+ (f32vector-ref (mesh.verts mesh) (+ i 0)) (coords.sx transform) (coords.x transform)))
                  (y (*+ (f32vector-ref (mesh.verts mesh) (+ i 1)) (coords.sy transform) (coords.y transform)))
                  )
                  (print! (number->string (/ i 4)) x y)
                  )
                ))
            )

          )
        ))
    ))

#|
(define mes (mesher
  (make-proto
    '(obj-file
      "content/graffitis/more.obj")
    '(img-file
      "content/graffitis/more.png")
    '(draw-settings
      (#t #f #t))
    '(transform
      (300 200 0 40 40))
    `(moves
      (
        ((12 13) ,(rads 70) 3)
        ((0 1) ,(rads 135) 1)
      ))
    )))
|#

(define (dna-plain args)
  (let* (
    (wh (get-dimensions))
    (w (car wh))
    (h (cdr wh))

    (ymin -40)
    (ymax (+ h 40))
    (yrange (- ymax ymin))

    (colora (or (args 'c1) '(0 0 1)))
    (colorb (or (args 'c2) '(1 0 1)))

    (over (or (args 'over) 30))
    (wide (or (args 'width) 30))
    (high (or (args 'height) 8))

    (bar-ct (or (args 'ct) 25))
    (v-spd (or (args 'spd) 10))

    (bar-spc (/ yrange bar-ct))

    (bars (make-q bar-ct
            (lambda (i)
              (cons (coinflip)
                    (+ (* i bar-spc) ymin))
              )))
    )
    (lambda ()
      (map
        (lambda (bar)
          (set-cdr! bar (- (cdr bar) v-spd)))
        (queue.as-list bars))
      (when (<= (cdar (queue.head bars)) ymin)
        (let (
          (it (queue.pop! bars))
          )
          (set-car! it (coinflip))
          (set-cdr! it (- ymax (- ymin (cdr it))))
          (queue.push! bars it)))
      (map
        (lambda (bar)
          (if (car bar)
            (apply set-color! colora)
            (apply set-color! colorb))
          (rect! over (cdr bar) wide high))
        (queue.as-list bars))
    )))

(define (dna-spin args)
  (let* (
    (wh (get-dimensions))
    (w (car wh))
    (h (cdr wh))

    (ymin -40)
    (ymax (+ h 40))
    (yrange (- ymax ymin))

    (bar-ct 15)
    (bar-spc (/ yrange bar-ct))

    (width (or (args 'w) 90))
    (phs-offset (* pi 0.6))
    (r-spd 0.3)
    (v-spd (or (args 'vspd) 12))
    (turns 3)
    (strs #("at" "gc"))

    (bars (make-q bar-ct
            (lambda (i)
              (cons (rand-in strs)
                    (+ (* i bar-spc) ymin))
              )))

    (draw-bar
      (lambda (bar phs)
        (let* (
          (x1 (sin phs))
          (x2 (sin (+ phs phs-offset)))
          (over (> x2 x1))
          (x1 (*+ x1 width (+ 20 width)))
          (x2 (*+ x2 width (+ 20 width)))
          (y (cdr bar))
          (text
            (if (> x1 x2)
                (car bar)
                (string-reverse (car bar))))
          )
          (set-color! 1 1 1)
          (rect! x1 (- y 5) (- x2 x1) 9 'line)
          (rect! x1 (- y 5) (/ (- x2 x1) 2) 9)
          (print! text (*+ width 2 40) (- y 10) 0 3)

          (circ! x1 y 9)
          (circ! x2 y 9)

          (set-color! 0.04)
          (circ! x1 y 7)
          (circ! x2 y 7)
          )))

    (g-phs 0)
    )
    (lambda ()
      (+=! g-phs r-spd)
      (set-shader!)
      (map
        (lambda (bar)
          (set-cdr! bar (- (cdr bar) v-spd))
          (let* (
            (ratio (* pi turns (/ (- (cdr bar) ymin) yrange)))
            )
            (draw-bar bar (+ ratio g-phs))
          ))
        (queue.as-list bars))
      (when (<= (cdar (queue.head bars)) ymin)
        (let (
          (it (queue.pop! bars))
          )
          (set-car! it (rand-in strs))
          (set-cdr! it (- ymax (- ymin (cdr it))))
          (queue.push! bars it)
          ))
    )))

(define (chemicals args)
  (let* (
    (F (make-framelimiter (or (args 'F) 1)))
    (R-str-len (make-rng 4 7))
    (chain-ct (or (args 'ct) 8))
    (v-spc 25)
    (hang 4)
    (hard-max 18)
    (base-x 60)
    (rx
      (lambda ()
        (* (rand 0 32) 15)))

    (make-glyph
      (lambda (str #!optional (offset 0) must-have-next)
        (make-proto
          `(str ,str)
          `(offset ,offset)
          `(must-have-next ,must-have-next)
          )))

    (starts "abcj")
    (chems
      (apply make-proto
        (map
          (lambda (g)
            (list (symbol->string (car g))
                  (apply make-glyph (cdr g))))
         '(
          (a "ccddddegg")
          (b "ppk")
          (c "aaddddeggkk")
          (d "aabbccdjj")
          (e "")
          (g "ggggieeek")
          (i "iiiiigeeek" 0 #t)
          (j "lnmk" 7)
          (k "c" 0 #t)
          (l "acj" 0 #t)
          (m "")
          (n "acccj" 0 #t)
          (p "ccb" 0 #t)
          )
         )))

    (make-chain
      (lambda (str x y)
        (make-proto
          `(x ,x)
          `(y ,y)
          `(str ,str)
          `(at ,(rand (len str)))
          `(go-up ,(coinflip))
          )))

    (random-str
      (lambda (curr-char)
        (let (
          (len (R-str-len))
          (str curr-char)
          )
          (let loop (
            (n 1)
            )
            (define nexts ((chems curr-char) 'str))
            (define have-next ((chems curr-char) 'must-have-next))
            (unless (or (> n hard-max)
                        (and (not have-next) (> n len))
                        (= (string-length nexts) 0))
              (let (
                (ch (rand-in nexts))
                )
                (..=! str ch)
                (set! curr-char ch))
              (loop (+ n 1))
              )
            )
          str)))

    (chains
      (make-vec chain-ct
        (lambda (i)
          (make-chain (random-str (rand-in starts))
                      (rx)
                      (* (- i 1) v-spc))
          )))
    )
    (map
      (lambda (pair)
        (let (
          (str (car pair))
          (gly (cdr pair))
          )
          (gly 'image
            (make-image
              (string-append "content/chems/" str ".png")))
          (gly 'tex
            (make-texture (gly 'image)))
          (gl:bind-texture gl:+texture-2d+ (gly 'tex))
          (texture-set-filter! gl:+texture-2d+ gl:+nearest+ gl:+nearest+)
          (gl:bind-texture gl:+texture-2d+ 0)
          ))
      (chems '_pairs))

    (lambda ()
      ; (set-color! 0.1 0.1 0.1 1)
      (when (F)
        (vector-map
          (lambda (c)
            (if (c 'go-up)
                (begin
                  (c 'at (+ (c 'at) 1))
                  (when (>= (c 'at) (+ hang (len (c 'str))))
                    (c '_clear! 'go-up)
                    ))
                (begin
                  (c 'at (- (c 'at) 1))
                  (when (< (c 'at) 0)
                    (c 'str (random-str (rand-in starts)))
                    (c 'x (rx))
                    (c 'go-up #t)
                    (c 'at 0)))
              ))
          chains))

      ; (push! coords-stack (make-coords -175 53 0 2))
      (vector-map
        (lambda (c)
          (let (
            (x (+ (c 'x) base-x))
            (y (c 'y))
            (str (safe-substring (c 'str) 0 (c 'at)))
            )
            (for 0 (len str) 1
              (lambda (i)
                (let* (
                  (ch (substring str i (+ i 1)))
                  (tex ((chems ch) 'tex))
                  (img ((chems ch) 'image))
                  (offset ((chems ch) 'offset))
                  )
                  (draw! tex x y)
                  (+=! x (- (image.w img) offset 1))
                  )))
            ))
        chains)
      ; (pop! coords-stack)
        )
    ))

(define (protozoa args)
  (let* (
    (prob (or (args 'prob) 0.02))
    (scl (or (args 'scl) 1.8))
    (C (or (args 'C) bodily))
    (xr 0.004)
    (yr 0.004)
    (xs 10)
    (ys 10)
    (R-spin-spd (apply make-rng (or (args 'spd) '(3.0 9.0))))
    (R-mov (make-rng 1.0 15.0))
    (R-mov (lambda () 0))
    (P-reverse (make-rpg 0.5))

    (pts
      (let* (
        (wh (get-dimensions))
        (w (car wh))
        (h (cdr wh))
        (nx (rand 1 100))
        (ny (rand 1 100))
        (collect '())
        )
        (for 0 (/ w xs) 1
          (lambda (xi)
            (for 0 (/ h ys) 1
              (lambda (yi)
                (when (< (rand)
                         (* prob (/+ (perlin (+ nx (* xi xr))
                                             (+ ny (* yi yr)))
                                 2 .5))
                      )
                  (let (
                    (pt (make-point (* xi xs)
                                    (* yi ys)))
                    )
                    (pt 'r (* (rand 0 3) 90))
                    (pt 'img (rand-in bacterias))
                    (pt 'phs (rand 0 360))
                    (pt 'rate ((if (P-reverse) - +) (R-spin-spd)))
                    (pt 'mov (R-mov))
                    (pt 'turn (rand 80 110))
                    ; (pt 'c (mix-color (rand-in C) C-white))
                    (pt 'c C-white)
                  (set! collect (cons pt collect))))
                  ))))
          (list->vector collect)))
    )
    (vector-shuffle! pts)
    (lambda ()
      (set-color! 1 1 1)
      (vector-map
        (lambda (pt)
          (let* (
            (xy (to-cartesian (pt 'mov) (rads (pt 'phs))))
            (x (+ (pt 'x) (car xy)))
            (y (+ (pt 'y) (cdr xy)))
            )
            (pt 'phs (+ (pt 'phs) (pt 'rate)))
            ; xxx this is slow lol
            (apply set-color! (pt 'c))
            (draw! (pt 'img) x y
              (rads (+ (pt 'r) (pt 'phs) (pt 'turn))) scl)
            ))
        pts)
      )
    ))

(define (wind args)
  (let* (
    (wh (get-dimensions))
    (w (car wh))
    (h (cdr wh))
    (pt-ct (rand 45 60))
    (R-small (make-rng 10.0 18.0))
    (R-big (make-rng 20.0 50.0))
    (P-big (make-rpg 0.7))
    (wind (apply rand (or (args 'wind) '(-8 8))))
    (fall (apply rand (or (args 'fall) '(2 4))))
    (mult 2)
    (diff 1)
    (pts
      (make-vec pt-ct
        (lambda (i)
          (let (
            (r (make-point (rand w) (rand h)))
            )
            (r 'r ((if (P-big) R-big R-small)))
            (r 'w (* (r 'r) diff))
            (r 'c (mix-color (rand-in mold) (rand-in diatoms) (*+ (rand) 0.5 0.5)))
            r))
        ))
    )
    (sort! pts
      (lambda (a b)
        (> (a 'r) (b 'r))))
    (set-shader!)
    (lambda ()
      (vector-for-each
        (lambda (p)
          (p 'x (+ (p 'x) (* (/ 2 (p 'w)) wind mult)))
          (p 'y (+ (p 'y) (* (/ 2 (p 'w)) 4 fall mult)))
          (p 'x (wrap (p 'x) (- (p 'r)) (+ w (p 'r))))
          (p 'y (wrap (p 'y) (- (p 'r)) (+ h (p 'r))))
          (apply set-color! (p 'c))
          (circ! (p 'x) (p 'y) (p 'r)))
        pts))
    ))

; todo make-vec can take inexact ?

; todo use args
(define (waves args)
  (let* (
    (pts (gen-noise-pts 0.004 6 0.004 6 0.018))
    (ngx (make-ngen (rand 1 100) 0.010))
    (ngy (make-ngen (rand 1 100) 0.010))
    (my 80)
    (mx 80)
    )
    (vector-map
      (lambda (pt)
        (pt 'img (rand-in creatures))
        (pt 'r (* (rand) 2pi))
        (pt 'sz (*+ (rand) 0.75 0.75))
        )
      pts)
    (vector-shuffle! pts)
    (lambda ()
      (let (
        (xexp 0)
        (yexp 0)
        )
        (set-shader!)
        (set-color! 1 1 1)
        ((ngx 'update))
        ((ngy 'update))
        (vector-for-each
          (lambda (pt)
            (let* (
              (x (pt 'x))
              (y (pt 'y))
              (xmov ((ngx 'get) (/ x 400)))
              (ymov ((ngy 'get) (/ y 400)))
              (fxmov (* xmov mx))
              (fymov (* ymov my yexp))
              #| todo angular velocity
              (xchg (- fxmov (or (pt 'last-fxmov) 0)))
              (ychg (- fymov (or (pt 'last-fymov) 0)))
              (a
                (if (= xchg 0)
                    (if (> ychg 0) 0 pi)
                    (cdr (to-polar xchg ychg))))
              |#
              )
              (draw! (pt 'img)
                (*+ (+ x fxmov) 1.25 -100)
                (*+ (+ y fymov) 1.5 -150)
                (pt 'r)
                (pt 'sz)
                )

              (pt 'last-fxmov fxmov)
              (pt 'last-fymov fymov)
              (+=! xexp (/ xmov 4))
              (+=! yexp (/ ymov 4))

              #|
              (print!
                (sprintf "~a ~a ~a"
                  a fxmov fymov)
                (*+ (+ x fxmov) 1.25 -100)
                (*+ (+ y fymov) 1.5 -150)
                )
              |#
              #|
              (circ!
                (*+ (+ x (* xmov mx)) 1.25 -100)
                (*+ (+ y (* ymov my yexp)) 1.5 -150)
                5)
              |#

              ))
          pts)
      )
    )))

#|
(define x (waves #f))
(define (frame)
  (x))
|#

(define (gradient args)
  (let* (
    (c1 (args 'c1))
    (c2 (args 'c2))
    (vert (args 'vert))
    (prog
      (make-program
        (make-frag-string
          "
          uniform vec3 c1;
          uniform vec3 c2;
          uniform bool vert;

          vec4 effect() {
            float at;

            if (vert)
              at = uv_coord.x;
            else
              at = uv_coord.y;

            vec3 color = hcy(mix(c1, c2, at));
            return vec4(color.rgb, 1.0);
          }
          ")))
    (tx (make-texture))
    (c1-loc (safe-get-uniform-location prog "c1"))
    (c2-loc (safe-get-uniform-location prog "c2"))
    (v-loc (safe-get-uniform-location prog "vert"))
    )
    (set-shader! prog)
    (when c1-loc
      (apply gl:uniform3f c1-loc c1))
    (when c2-loc
      (apply gl:uniform3f c2-loc c2))
    (when v-loc
      (gl:uniform1i v-loc (if vert 1 0)))
    (lambda ()
      (set-shader! prog)
      (draw-px! default-texture 0 0 0 window-width window-height)
      )))

(define (rays args)
  (let* (
    (c1 (or (args 'c1) '(1)))
    (c2 (or (args 'c2) '(0)))
    (spd (or (args 'spd) 0.004))
    (ct (or (args 'ct) 6))
    (fn1 (or (args 'fn1) +))
    (fn2 (or (args 'fn2) +))
    (total-ct (* ct 2))
    (offset (/ 2pi total-ct))
    (sz (get-radius))
    (cc (get-center))
    (cx (car cc))
    (cy (cdr cc))
    (phs 0)
    )
    (lambda ()
      (set-shader!)
      (+=! phs spd)
      (for 0 total-ct 1
        (lambda (n)
          (let* (
            (one (to-cartesian sz (+ (fn1 phs) (* n offset))))
            (two (to-cartesian sz (+ (fn2 phs) (* (+ n 1) offset))))
            (x1 (+ cx (car one)))
            (y1 (+ cy (cdr one)))
            (x2 (+ cx (car two)))
            (y2 (+ cy (cdr two)))
            )
            (if (even? n)
                (apply set-color! c1)
                (apply set-color! c2))
            (tri! cx cy x1 y1 x2 y2)
            ))
        ))
  ))

(define (anenomie args)
  (let* (
    (rate (or (args 'rate) 0.04))
    (start (or (args 'start) (rand 0 100)))
    (prog
      (make-program
        (make-frag-string
        "
        uniform float time;

        vec4 circ(float x, float y, float r, float co) {
          float c = length(vec2(x, y) - uv_coord);
          c = smoothstep(r, r+0.03, c);
          c = 1 - c;
          return vec4(c * hcy(vec3(co, 0.8, 0.2)), c);
        }

        vec4 draw_ring(float phs, float r, int ct, float sz) {
          float spc = PI * 2 / ct;
          float angle = 0;
          vec4 o = vec4(0,0,0,0);
          for (int i = 0; i < ct; ++i) {
            float x = r * cos(angle + i * spc + phs);
            float y = r * sin(angle + i * spc + phs);
            o += circ(x + 0.5, y + 0.5, sz, 1-abs(tan(time/5)) * (float(i)/float(ct)/2+0.55));
          }

          return o;
        }

        vec4 effect() {
          int r_ct = 10;
          float r_diff = 0.06;
          float r_scl = 0.055;

          vec4 o = vec4(0, 0, 0, 0);
          for (int i = 0; i < r_ct; ++i) {
            float phs = i * r_diff * time;
            phs = sin(phs) + phs*0.75 + time/12;
            vec4 r = draw_ring(phs, i * r_scl + 0.1, (i + 1) * 2, 0.021); 
            o = vec4(max(o.r, r.r) ,
                     max(o.g, r.g) ,
                     max(o.b, r.b) ,
                     (o.a + r.a));
          }
          float oa = o.a;
          o.a /= 1.5;
          // o.a = sqrt(o.a);
          o.a = 2 * (0.5 - abs(o.a - 0.5));
          o.a = oa / 1 + smoothstep(0.3, 0.6, o.a);

          return o;
        }
        "
        )))
    (tloc (safe-get-uniform-location prog "time"))
    (t start)
    )
    (lambda (msg . args)
      (cond
        ((eq? msg 'update)
          (+=! t rate))
        ((eq? msg 'reset)
          (set! t (car args)))
        ((eq? msg 'draw)
          (set-shader! prog)
          (when tloc
            (gl:uniform1f tloc t))
          (draw-px! default-texture 0 -100))
        )
      )))

; todo
; better corners
;   take a 0-1 for angle
; placemnet corner fn
; blood amt param
(define (blood args)
  (let* (
    (bloods '())
    (P-bleed (make-rpg 0.1))
    (P-big (make-rpg 0.2))
    (prog
      (make-program
        ; vert shader is do you dont floor coords
        ;   use a uniform in default shader?
        "#version 330 core
        layout (location = 0) in vec2 vertex;
        layout (location = 1) in vec2 uv;

        uniform mat4 proj;
        uniform vec2 pos;
        uniform float rot;
        uniform vec2 scl;

        mat4 model;

        out vec2 uv_coord;

        vec4 position() {
          return proj * model * vec4(vertex.xy, 0.0, 1.0);
        }

        void main() {
          uv_coord = uv;

          model = mat4(1.0);
          float rc = cos(rot);
          float rs = sin(rot);
          model[0][0] =  rc * scl.x;
          model[0][1] =  rs * scl.x;
          model[1][0] = -rs * scl.y;
          model[1][1] =  rc * scl.y;
          model[3][0] = pos.x;
          model[3][1] = pos.y;

          gl_Position = position();
        }
        "
        (make-frag-string
          "
          uniform float time;
          vec4 effect() {
            vec2 dist = vec2(0.5, 0.5) - uv_coord;
            float x = dist.x;
            float y = dist.y;
            float z = smoothstep(0.5, 0.75, 1-length(dist));

            vec3 norm = normalize(vec3(x, y, z));

            // vec3 dir = normalize(vec3(0, fract(time), 0.5));
            float dx = cos(snoise(vec2(time/2,0))/2+1);
            float dy = sin(snoise(vec2(time,0))/2+1);
            vec3 dir = vec3(dx, dy, 0.6);

            vec3 sdir = reflect(-dir, norm);

            float l = max(dot(norm, dir), 0);
            float spec = pow(max(dot(norm, sdir), 0), 16);
            spec = min(spec, 1);
            // spec = 2 * (0.5 - abs(spec - 0.5));
            float a =  1- smoothstep(0.4, 0.45, length(dist));

            vec3 c1 = vec3(1, 0, 0);
            vec3 c2 = vec3(0.6, 0.1, 0.1);
            return vec4(max(vec3(1, 0.4, 0.4) * spec, mix(c2, c1, l)), a);
          }
          ")))

    (tloc (safe-get-uniform-location prog "time"))
    (t 0)
    (ns (make-ngen 0 0.03))
    (wr (get-radius))
    )
    (lambda (msg)
      (cond
        ((eq? msg 'update)
          (+=! t 0.04)
          ((ns 'update))
          (when (P-bleed)
            (let (
              (circ
                (let (
                  (b (P-big))
                  )
                (make-proto
                  `(loc ,(pt-in-corner (* (*+ ((ns 'get)) 1 0.5) wr) 'tl))
                  `(sz ,(if b (*+ (rand) 20 30) 5))
                  `(spd ,(if b 0.9 0.3))
                  )))
              )
              (set! bloods (append bloods (list circ)))
              ))
          (for-each
            (lambda (c)
              (let (
                (r (c 'sz))
                )
                (c 'sz (+ (* (c 'spd) (/ 80 r)) r))
                ))
            bloods)
          )
        ((eq? msg 'reset)
          (set! bloods '()))
        ((eq? msg 'draw)
          (set-shader! prog)
          (set-color! 1 0 0)
          (when tloc
            (gl:uniform1f tloc t))
          (for-each
            (lambda (c)
              (let (
                (x (car (c 'loc)))
                (y (cdr (c 'loc)))
                (r (c 'sz))
                )
                (draw-px! default-texture (- x (/ r 2)) (- y (/ r 2)) 0 r)
                ))
            bloods)
          )
        ))
    ))

(define (color-bars args)
  (let* (
    (nspd 0.04)
    (ndiff 0.1)
    (min 15)
    (max 80)
    (R-width (make-rng 0.5 1.05))
    (C (or (args 'C) diatoms))
    (ct 100)
    (colors (make-vec ct
              (lambda (i)
                (rand-in C)
                )))
    (w-mult (make-vec ct
              (lambda (i)
                (R-width)
                )))
    (range (- max min))
    (noisex (rand 0 100))
    )
    (lambda ()
      (+=! noisex nspd)
      (let (
        (x 0)
        )
        (for 0 ct 1
          (lambda (n)
            (let* (
              (idx (+ (* ndiff n) noisex))
              (w (* (vector-ref w-mult n)
                    (*+ (/+ (perlin idx 0) 2 0.5) range min)))
              )
              (apply set-color! (vector-ref colors n))
              (rect! x 0 w 600)
              (+=! x w)
              )))
        ))
  ))

#|
(define cb (color-bars))

(define (frame)
  (cb))
|#
