(use (srfi 133))

(define (make-proto . args)
  (let (
    (ht (make-hash-table))
    )
    (map
      (lambda (a)
        (hash-table-set! ht (car a) (cadr a)))
      args)
    (lambda (message #!optional to)
      (case message
        ((_ht)
          ht)
        ((_pairs)
          (hash-table->alist ht))
        ((_clear!)
          (hash-table-set! ht to #f))
        (else
          (if to
              (hash-table-set! ht message to)
              (hash-table-ref/default ht message #f)))
        ))
    ))

(define ($f lst)
  (map (cut / <> 255) lst))

(define (make-framelimiter ct)
  (let (
    (acc 0)
    )
    (lambda ()
      (set! acc (+ acc 1))
      (if (>= acc ct)
          (begin
            (set! acc 0)
            #t)
          #f))))

(define (make-rpg #!optional prob)
  (let (
    (prob (or prob 0.5))
    )
    (lambda ()
      (> prob (rand)))))

(define (make-rng . args)
  (if (every exact? args)
      (lambda ()
        (apply rand args))
      (lambda ()
        (let (
          (r (rand))
          (min (car args))
          (max (cadr args))
          )
          (*+ r (- max min) min)))
      ))

(define (len of)
  (cond
    ((vector? of)
      (vector-length of))
    ((pair? of)
      (length of))
    ((string? of)
      (string-length of))
    ((queue? of)
      (length (queue.as-list of)))
    (else
      (error "cannot take len"))
    ))

(define (make-vec ct fn)
  (let (
    (v (make-vector ct))
    )
    (for 0 ct 1
      (lambda (i)
        (vector-set! v i (fn i))))
    v))

(define (wrap x min max)
  (cond
    ((< x min)
      (- max (- min x)))
    ((> x max)
      (+ min (- x max)))
    (else x)))

(define (7up arg)
  (dnL arg)
  arg)

;

; todo generalize
(define (load-directory loc)
  (let (
    (files (directory loc))
    )
    (apply vector
      (map
        (lambda (x)
          (let* (
            (file (make-pathname loc x))
            (img (make-image file))
            (tex (make-texture img))
            )
            tex))
        files))
    ))

; todo how to know video size in a nicer way
;   check for events here (probably)
; also make-image needs to know that the file exists
(define (load-frames directory from to)
  (let (
    (start (glfw:get-time))
    (ret
      (make-vec (- to from)
        (lambda (i)
          (let* (
            (img
              (make-image
                (sprintf "~a/~a.png"
                  directory
                  (number->string (+ i from))
                  )))
            (tex (make-texture img))
            )
            (glfw:poll-events)
            (when (glfw:window-should-close (glfw:window))
              (dnL 'quitting)
              (exit 1)
              )
            (when (even? i)
              (clear-line!)
              (display
                (sprintf "loadin' ~a%"
                  (nice% (/ i (- to from)))
                  ))
              (flush-output)
              )
            (usleep 10)
            (image.free-data! img)
            (cons img tex)
            ))))
    )
    (clear-line!)
    (display "loaded 100%")
    (newline)
    (glfw:set-time start)
    ret))

(define (safe-substring str i j)
  (let (
    (j (min j (string-length str)))
    )
    (substring str i j)))

(define (vector-shuffle! vec)
  (for 0 (/ (len vec) 2) 1
    (lambda (i)
      (let (
        (tmp (vector-ref vec i))
        (at (rand (len vec)))
        )
        (vector-set! vec i (vector-ref vec at))
        (vector-set! vec at tmp))))
  )

(define _temp_fb_vec (make-s32vector 1))

(define (framebuffer.tex fb)
  (gl:bind-framebuffer gl:+framebuffer+ fb)
  (gl:get-framebuffer-attachment-parameteriv
    gl:+framebuffer+
    gl:+color-attachment0+
    gl:+framebuffer-attachment-object-name+
    _temp_fb_vec)
  (gl:bind-framebuffer gl:+framebuffer+ 0)
  (s32vector-ref _temp_fb_vec 0)
  )

;

(define-struct queue
  (mut head)
  (mut tail))

(define-record-printer (queue obj port)
  (fprintf port
    "#queue(~a . ~a)"
    (queue.head obj)
    (queue.tail obj)
    ))

(define (make-queue)
  (_make-queue '() '()))

(define (make-q ct fn)
  (let (
    (ret (make-queue))
    )
    (for 0 ct 1
      (lambda (i)
        (queue.push! ret (fn i))))
    ret))

(define (queue.empty? q)
  (null? (queue.head q)))

(define (queue.push! q obj)
  (let (
    (ptr (list obj))
    )
    (if (queue.empty? q)
        (begin
          (queue.head! q ptr)
          (queue.tail! q ptr))
        (begin
          (set-cdr! (queue.tail q) ptr)
          (queue.tail! q ptr)))
    ))

(define (queue.pop! q)
  (if (queue.empty? q)
      #f
      (let (
        (ret (car (queue.head q)))
        )
        (queue.head! q (cdr (queue.head q)))
        (when (null? (queue.head q))
          (queue.tail! q '()))
        ret)))

(define queue.as-list queue.head)

;

(define-struct anim
  (immut frames)
  (mut at))

(define (make-anim f)
  (_make-anim f 0))

(define (anim.peek a)
  (vector-ref (anim.frames a) (anim.at a)))

(define (anim.next! a)
  (anim.at! a (+ (anim.at a) 1))
  (when (>= (anim.at a) (vector-length (anim.frames a)))
    (anim.at! a 0))
  (anim.peek a))

(define (anim.prev! a)
  (anim.at! a (- (anim.at a) 1))
  (when (< (anim.at a) 0)
    (anim.at! a (- (vector-length (anim.frames a)) 1)))
  (anim.peek a))

(define (anim.reset! a #!optional at)
  (let* (
    (at (or at 0))
    (at (modulo at (vector-length (anim.frames a))))
    )
    (anim.at! a at)))

;

(define-struct stack
  (mut lst))

(define (make-stack)
  (_make-stack '()))

(define (stack.push! s val)
  (stack.lst! s (cons val (stack.lst s))))

(define (stack.pop! s)
  (if (null? (stack.lst s))
      #f
      (let (
        (ret (car (stack.lst s)))
        )
        (stack.lst! s (cdr (stack.lst s)))
        ret)))

(define (stack.peek s)
  (if (null? (stack.lst s))
      #f
      (car (stack.lst s))))

(define (stack.clear! s)
  (stack.lst! '()))

;

(define (mix-color a b #!optional amt)
  (let* (
    (amt (or amt 0.5))
    (inv (- 1 amt))

    (ar (car a))
    (ag (cadr a))
    (ab (caddr a))
    (aa (if (null? (cdddr a)) 1 (cadddr a)))

    (br (car b))
    (bg (cadr b))
    (bb (caddr b))
    (ba (if (null? (cdddr b)) 1 (cadddr b)))
    )
    (list
      (+ (* ar inv) (* br amt))
      (+ (* ag inv) (* bg amt))
      (+ (* ab inv) (* bb amt))
      (+ (* aa inv) (* ba amt))
      )))

(define (make-point x y)
  (make-proto
    `(x ,x)
    `(y ,y)
    ))

; x & yvar are how fast u scan over the noise
; x & yscl are where those pts r mapped
; todo idt u need both but idk
(define (gen-noise-pts xvar xscl yvar yscl prob)
  (let* (
    ; todo gen in rect / gen in 0 - 1
    (wh (get-dimensions))
    (w (car wh))
    (h (cdr wh))
    (lst '())
    )
    (let (
      (nx (rand 1 100))
      (ny (rand 1 100))
      (xct (inexact->exact (floor (/ w xscl))))
      (yct (inexact->exact (floor (/ h yscl))))
      )
      (for 0 xct 1
        (lambda (xi)
          (let (
            (nxi (*+ xi xvar nx))
            )
            (for 0 yct 1
              (lambda (yi)
                (let (
                  (nyi (*+ yi yvar ny))
                  )
                  (when (< (rand) (* (*+ (perlin nxi nyi) 0.5 0.5) prob))
                    (let (
                      (p (make-point (* xi xscl) (* yi yscl)))
                      )
                      (set! lst (cons p lst))
                      ))
                  )))
            )))
      )
    (reverse-list->vector lst)
    ))

(define (make-ngen offset n-spd)
  (let (
    (p (make-proto
         '(ns-at 0)
         `(n-spd ,n-spd)
         `(offset ,offset)
         ))
    )
    (p 'update
      (lambda ()
        (p 'ns-at (+ (p 'ns-at) (p 'n-spd)))))
    (p 'get
      (lambda (#!optional at)
        (let (
          (at (or at 0))
          )
          (perlin (+ at (p 'ns-at)) (p 'offset)))))
    p))

; todo position in corner
(define (pt-in-corner dist corner)
  (let (
    (mult
      (case corner
        ((bl) 3)
        ((tl) 4)
        ((br) 2)
        ((tr) 1)
        (else 0)
        ))
    )
    (to-cartesian dist (* (+ (rand) mult) pi/2)))
  )

;

; todo do mesh in CPP someday
(define-struct mesh
  (immut vao)
  (immut vbo)
  (immut ebo)
  (immut verts)
  (immut indcs)
  ; (mut draw-wireframe)
  ; (mut draw-texture)
  )

; verts are x y u v
; mesh is 2D
(define (make-mesh verts indcs draw-type)
  (let (
    (vao (make-vao))
    (vbo (make-buffer))
    (ebo (make-buffer))
    )
    (gl:bind-vertex-array vao)
    (gl:bind-buffer gl:+array-buffer+ vbo)
    (gl:bind-buffer gl:+element-array-buffer+ ebo)

    (gl:buffer-data gl:+array-buffer+ (* 4 (f32vector-length verts))
      (make-locative verts) draw-type)
    (gl:buffer-data gl:+element-array-buffer+ (* 4 (u32vector-length indcs))
      (make-locative indcs) gl:+static-draw+)

    (gl:vertex-attrib-pointer 0 2 gl:+float+ #f (* 4 4) (address->pointer 0))
    (gl:vertex-attrib-pointer 1 2 gl:+float+ #f (* 4 4) (address->pointer (* 2 4)))
    (gl:enable-vertex-attrib-array 0)
    (gl:enable-vertex-attrib-array 1)

    (gl:bind-vertex-array 0)

    (_make-mesh vao vbo ebo verts indcs)))

(define (load-obj-file filename draw-type)
  (let* (
    (lines (read-lines filename))
    (cts
      (fold-right
        (lambda (ln acc)
          (cond
            ((char=? (string-ref ln 0) #\v)
              (if (char=? (string-ref ln 1) #\t)
                  (cons* (car acc) (add1 (cadr acc)) (cddr acc))
                  (cons* (add1 (car acc)) (cadr acc) (cddr acc))
                  ))
            ((char=? (string-ref ln 0) #\f)
              (cons* (car acc) (cadr acc) (add1 (cddr acc))))
            (else
              acc)))
        '(0 0 . 0)
        lines))
    (v-ct (car cts))
    (vt-ct (cadr cts))
    (f-ct (cddr cts))
    (vts (make-vector vt-ct))
    (v-out (make-f32vector (* 4 v-ct)))
    (i-out (make-u32vector (* 3 f-ct)))
    (vs-at 0)
    (vts-at 0)
    (indc-at 0)
    )
    (for-each
      (lambda (ln)
        (cond
          ((char=? (string-ref ln 0) #\o)
            (cons 'o
              (substring ln 2 (string-length ln))))
          ((char=? (string-ref ln 0) #\v)
            (let (
              (val (map string->number
                     (irregex-extract 'real ln)))
              )
              (if (char=? (string-ref ln 1) #\t)
                  (begin
                    (vector-set! vts vts-at val)
                    (+=! vts-at 1))
                  (begin
                    (f32vector-set! v-out (*+ vs-at 4 0) (car val))
                    (f32vector-set! v-out (*+ vs-at 4 1) (- (cadr val)))
                    (+=! vs-at 1)))
              ))
          ((char=? (string-ref ln 0) #\f)
            (let (
              (st (map string->number (irregex-extract 'integer ln)))
              )
              (u32vector-set! i-out (*+ indc-at 3 0) (- (list-ref st 0) 1))
              (u32vector-set! i-out (*+ indc-at 3 1) (- (list-ref st 2) 1))
              (u32vector-set! i-out (*+ indc-at 3 2) (- (list-ref st 4) 1))
              (+=! indc-at 1)
              (let loop (
                (lst st)
                )
                (unless (null? lst)
                  (let* (
                    (v   (- (car lst) 1))
                    (vt  (- (cadr lst) 1))
                    (vt* (vector-ref vts vt))
                    )
                    (f32vector-set! v-out (*+ v 4 2) (car vt*))
                    (f32vector-set! v-out (*+ v 4 3) (- 1 (cadr vt*)))
                    )
                  (loop (cddr lst)))
                )
              ))
            ))
        lines)
    (make-mesh v-out i-out draw-type)
    ))

(define (mesh.sub! mesh pt-at x y uvx uvy)
  (let* (
    (vs (mesh.verts mesh))
    (maybe-set!
      (lambda (offset to)
        (when to
          (f32vector-set! vs (*+ pt-at 4 offset) to))))
    )
    (maybe-set! 0 x)
    (maybe-set! 1 y)
    (maybe-set! 2 uvx)
    (maybe-set! 3 uvy)
    ))

(define (mesh.push-changes! mesh min max)
  (gl:bind-buffer gl:+array-buffer+ (mesh.vbo mesh))
  (gl:buffer-sub-data gl:+array-buffer+  (* 4 4 min) (* 4 4 (- max min))
    (make-locative (mesh.verts mesh)))
  )

;

(define (make-stacker items limit)
  (let* (
    (anim (make-anim items))
    (F (make-framelimiter limit))
    (at 0)
    )
    (lambda (#!optional msg)
      (cond
        ((or (not msg) (eq? msg 'next))
          (anim.next! anim))
        ((eq? msg 'update)
          (when (F)
            (+=! at 1))
          (anim.reset! anim at))
        ))
    ))
