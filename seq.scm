(define clips (make-hash-table))
(define (at num clip)
  (hash-table-update!/default clips num
    (lambda (lst)
      (cons clip lst)
      )
    '())
  )
(load-once curr-clips #f)

(define (call fn)
  (fn))

(define is-first-frame #f)

(define (frame)
  (let (
    (val (hash-table-ref/default clips frame-at #f))
    )
    (if val
        (begin
          (set! curr-clips val)
          (set! is-first-frame #t))
        (set! is-first-frame #f)
      )
    (when curr-clips
      (for-each call curr-clips))
    ))

(define (blocks num #!optional b)
  (beats (*+ num 16 (or b 0)))
  )

;

(set-tempo! 190)
(set-audio! "2 u7.wav")
(set-export-frames! (blocks 0) (blocks 39 12))
(set-export-dir! "export")

; xxx poli play with cemicals ?

; todo
;   transition on the end
;   multiple sploches

; fns
;   adjustment
;   mixer
;   ripple
;   reaction diffusion
;   bar chart
;   bar circle
;   snois

;   black dots
;   mesher
;   dna plain
;   dna spin
;   chemicals
;   protozoa
;   wind
;   waves (protozoa)
;   gradient
;   rays
;   anenomie
;   blood
;   color bars
;   [x] vplayer
;   [x] mather

; 3D
;   blob poke
;   blob poke behind
;   bphage jump
;   bphage run at
;   bphage run circ
;   bphage run circ top
;   draw circs behind
;   draw circs front
;   farm 1
;   farm 2
;   farm alt 1 maybe better
;   farm alt 2
;   farm alt t 1
;   farm alt t 2
;   froghops
;   messy froghop
;   pokyu
;   pour far
;   pour trans
;   shiva
;   sleep 1
;   sleep 2
;   swimming
;   wt1
;   wt2

; vids
;   strep
;   ecoli
;   hidef

;

(define (clear-fb!)
  (gl:bind-framebuffer gl:+framebuffer+ 0))

(define-syntax with-adjustments
  (syntax-rules ()
    ((_ adj body ...)
      (begin
        (adj 'bind)
        (set-shader!)
        (set-color! 1)
        body ...
        (clear-fb!)
        (adj 'draw)
      ))))

(define (hide . args)
  0)

(define (bg-fill! color)
  (set-shader!)
  (apply set-color! color)
  (rect! 0 0 window-width window-height)
  )

; =================
; =================
; =================
; intro

(load-once Rshiva
  (load-frames "content/rec/shiva" 0 40))
(load-once Rtpour
  (load-frames "content/rec/pour trans/" 0 48))

(load-once static-graffiti
  (load-frames "content/static-graffitis" 1 9))

(define Ashiva (make-anim Rshiva))
(define Atpour (make-anim Rtpour))

(define poli-adj (adjustment (make-proto
  '(do-levels 1)
  '(brightness (1.2 1.2 1.2))
  '(contrast (1.05 1.05 1.05))

  '(do-hsl 1)
  '(hsl-adj (0.0 0.55 1))
  '(colorize 0)
  )))

(define m1 (mesher (make-proto
    '(obj-file
      "content/graffitis/more.obj")
    '(img-file
      "content/graffitis/more.png")
    '(draw-settings
      (#t #f #f))
    '(transform
      (300 200 0 40 40))
    `(moves
      (
        ((12 13) ,(rads 70) 3)
        ((0 1) ,(rads 135) 1)
      ))
    )))

(define protos (protozoa (make-proto
  '(scl 1.5)
  '(prob 0.02)
  `(C ,mold)
)))

(define rr (ripple))

(define stk (make-stacker `#(
  (,(cdr (vector-ref static-graffiti 0)) 300 100 ,(rads 90) 2)
  (,(cdr (vector-ref static-graffiti 5)) 120 200 0 2 1)
  (,(cdr (vector-ref static-graffiti 6)) 300 200 0 2)
  ) 5
))

(define (sideg)
  (stk 'update)

  (rr 'update)
  (rr 'bind)
  (m1 'draw)
  (clear-fb!)

  (push! coords-stack (make-coords 30 10 0 1.4 1.4))

  (set-shader!)
  (apply draw! (stk))
  (rr 'draw)
  (set-shader!)
  (apply draw! (stk))

  (pop! coords-stack)
  )

(define bdu (black-dots (make-proto
  '(c (0))
  '(sizes (4 5))
  )))

(define bd (black-dots (make-proto
  '(sizes (8 9))
  '(c (0))
)))

(define F0 (make-framelimiter 3))

(define bb (blood (make-proto
  )))

(define sn (snois (make-proto
  '(num 1)
  )))

(at 0 (lambda ()
  (when is-first-frame
    (anim.reset! Ashiva)
    )

  (with-adjustments poli-adj
    (draw! (cdr (anim.next! Ashiva)) -25 0 0 1.05 1.05)
    )
  ))

(at (beats 5) (lambda ()
  (with-adjustments poli-adj
    (draw! (cdr ((if (F0) anim.next! anim.peek) Ashiva))
      -25 0 0 1.05 1.05)
    )
  (set-shader!)
  (bdu)
  (protos)
  (bd)
  ))

(at (blocks 1) (lambda ()
  (when is-first-frame
    (bb 'reset))

  (with-adjustments poli-adj
    (draw! (cdr (anim.next! Ashiva)) -25 0 0 1.05 1.05)
    )
  (bb 'update)
  (sideg)
  (bb 'draw)
  ))

(at (blocks 1 5) (lambda ()
  (with-adjustments poli-adj
    (draw! (cdr ((if (F0) anim.next! anim.peek) Ashiva))
      -25 0 0 1.05 1.05)
    )

  (bb 'update)

  ; xxx different side grafiti ?

  (bb 'draw)
  ))

(at (blocks 2) (lambda ()
  (with-adjustments poli-adj
    (draw! (cdr (anim.next! Ashiva)) -25 0 0 1.05 1.05)
    )

  (sideg)
  ))

(at (blocks 2 5) (lambda ()
  (bg-fill! '(0))
  (sn)
  (sideg)
  ))

(loop!
  (blocks 0)
  (blocks 3))

; )

;; =========================
;; start
;; =====

(load-once Vecoli
  (load-frames "content/videos/ecoli" 1 100))
(load-once Rmessy-hop
  (load-frames "content/rec/jump behind" 76 118))
(load-once Rwt1
  (load-frames "content/rec/wt1" 0 23))
(load-once Rwt2
  (load-frames "content/rec/wt2" 0 23))
(load-once Rsidejumps
  (load-frames "content/rec/sidejumps" 74 100))

(define Aecoli (make-anim Vecoli))
(define Amessy-hop (make-anim Rmessy-hop))
(define Awt1 (make-anim Rwt1))
(define Awt2 (make-anim Rwt2))
(define Asidejumps (make-anim Rsidejumps))

(define anen (anenomie (make-proto
  '(rate 0.1)
  '(start 0)
  )))

(define rbg (rays (make-proto
  '(c1 (1 1 0 1))
  '(c2 (0 0 0 1))
  '(ct 3)
  '(spd -0.01)
  )))

(define dnspin (dna-spin (make-proto
  '(w 75)
  '(vspd 10)
  )))

(define dnpl1 (dna-plain (make-proto
  '(c1 (0 0.7 0.3))
  '(c2 (0.7 0.2 0.8))
  '(over 240)
  '(ct 15)
  '(height 20)
  '(width 40)
  '(spd 15)
  )))

(define dnpl2 (dna-plain (make-proto
  '(c1 (0 0.2 0.7 0.8))
  '(c2 (0.7 0.2))
  '(over 290)
  '(ct 12)
  '(height 20)
  '(width 20)
  '(spd 2)
  )))

(define cc (chemicals (make-proto
  '(ct 4)
  )))

(define cb (color-bars (make-proto
  )))

(define poli-sharp (adjustment (make-proto
  '(do-levels 1)
  '(brightness (1.2 1.2 1.2))
  '(do-hsl 1)
  '(hsl-adj (0.1 0.8 1))
  '(sharpen-amt 0.002)
  )))

(define sploch (black-dots (make-proto
  '(sizes (20 35))
  )))

(define sploch2 (black-dots (make-proto
  '(sizes (20 35))
  )))

(at (blocks 3) (lambda ()
  (when is-first-frame
    (anim.reset! Awt1)
    )

  (sn)

  (set-shader!)
  (anim.next! Awt1)
  (set-color! 0)
  (draw! (cdr (anim.peek Awt1)) -20 0)

  (set-color! 1)
  (with-adjustments poli-adj
    (draw! (cdr (anim.peek Awt1)))
    )
  ))

(at (blocks 3 7) (lambda ()
  (set-shader!)
  (set-color! 1)
  (cb)

  (sploch)

  (with-adjustments poli-sharp
    (draw! (cdr (anim.next! Awt2)) 60 0)
    )

  (set-shader!)
  (set-color! 1)
  (push! coords-stack (make-coords -175 53 0 2))
  (cc)
  (pop! coords-stack)
))

(at (blocks 4) (lambda ()
  (when is-first-frame
    (anim.reset! Amessy-hop)
    (anim.reset! Aecoli)
    )

  (when (> (anim.at Amessy-hop) 30)
    (anim.reset! Amessy-hop))

  (set-shader!)
  (set-color! 1)
  (draw-px! (cdr (anim.next! Aecoli)) 0 -3 0 640 484)
  (with-adjustments poli-adj
    (draw! (cdr (anim.next! Amessy-hop)))
    )
  (dnspin)
  (dnpl1)
  (dnpl2)
  ))

;

(at (blocks 5) (lambda ()
  (when is-first-frame
    (anim.reset! Awt1)
  )

  (rbg)

  (set-shader!)
  (anim.next! Awt1)
  (set-color! 0)
  (draw! (cdr (anim.peek Awt1)) -20 0)

  (set-color! 1)
  (with-adjustments poli-adj
    (draw! (cdr (anim.peek Awt1)))
    )
  ))

(define mxr (mixer (make-proto
  '(mode 1)
  )))

(define inv (adjustment (make-proto
  '(invert 1)
  )))

(define _b5_Cs '#(
  (0.5 1 0)
  (1 1 0)
  (1 0.5 0)
  (0 0 1)
  ))

(define _b5_F (make-framelimiter 4))
(define _b5_cs (make-anim _b5_Cs))

(at (blocks 5 8) (lambda ()
  (when is-first-frame
    (anen 'reset 0))

  (when (_b5_F)
    (anim.next! _b5_cs))

  (rbg)

  (anen 'update)
  (anen 'draw)

  (inv 'bind)
  (anim.next! Awt2)
  (set-shader!)
  (set-color! 0)
  (draw! (cdr (anim.peek Awt2)) 60 0)

  (mxr 'bind-a)
  (set-shader!)
  (set-color! 0)
  (rect! 0 0 window-width window-height)
  (inv 'draw)

  (mxr 'bind-b)
  (set-shader!)
  (apply set-color! (anim.peek _b5_cs))
  (rect! 0 0 window-width window-height)

  (clear-fb!)
  (mxr 'draw)
  ))

(define _b6_F (make-framelimiter 8))

(at (blocks 6) (lambda ()
  (when (_b6_F)
    (anim.next! _b5_cs))

  (set-shader!)
  (apply set-color! (anim.peek _b5_cs))
  (rect! 0 0 window-width window-height)

  (anim.next! Asidejumps)

  (set-shader!)
  (set-color! 1)
  (with-adjustments poli-adj
    (draw! (cdr (anim.peek Asidejumps)) 300 0 0 1 1)
    (draw! (cdr (anim.peek Asidejumps)) 150 0 0 1 1)
    (draw! (cdr (anim.peek Asidejumps)) 0 0 0 1 1)
    (draw! (cdr (anim.peek Asidejumps)) -150 0 0 1 1)
    (draw! (cdr (anim.peek Asidejumps)) -300 0 0 1 1)
    )
  (anen 'update)
  (anen 'draw)
  ))

(loop!
  (blocks 3)
  (blocks 7)
  )

; )

; =============================

(load-once Rblob-poke
  (load-frames "content/rec/blob poke" 0 25))
(load-once Rblob-poke2
  (load-frames "content/rec/blob poke behind" 0 25))
(load-once Rpokyu
  (load-frames "content/rec/pokyu" 0 24))
(load-once Rbpjump
  (load-frames "content/rec/bphage jump" 0 32))
(load-once Rbpctop
  (load-frames "content/rec/bphage run circ top" 0 140))
(load-once Rbpc
  (load-frames "content/rec/bphage run circ" 0 130))

(define Apokyu (make-anim Rpokyu))
(define Ablob-poke (make-anim Rblob-poke))
(define Ablob-poke2 (make-anim Rblob-poke2))
(define Abpjump (make-anim Rbpjump))
(define Abpctop (make-anim Rbpctop))
(define Abpc (make-anim Rbpc))

(define bp-adj (adjustment (make-proto
  '(do-hsl 1)
  '(hsl-adj (0.08 3 0.5))
  )))

(define _b7_F (make-framelimiter 12))
(define bc (bar-circles (make-proto
  `(C ,bodily)
  '(r 100)
  )))
(define Ww (wind (make-proto
  )))

(at (blocks 7) (lambda ()
  (set-shader!)
  (bc)

  (bdu)

  (with-adjustments poli-adj
    (draw! (cdr (anim.next! Ablob-poke)) -20 -50 0 1.18)
    )

  (bd)
  ))

(at (blocks 8) (lambda ()
  (Ww)

  (with-adjustments bp-adj
    (draw! (cdr (anim.next! Abpjump)) 160 0)
    )

  (dnspin)
  (dnpl1)
  (dnpl2)
  ))

(at (blocks 9) (lambda ()
  (when is-first-frame
    (anim.reset! Abpctop)
    (anim.reset! Abpc 10)
    )

  (set-shader!)

  (bc)

  (Ww)

  (with-adjustments poli-adj
    (draw! (cdr (anim.next! Ablob-poke)) -20 -50 0 1.18)
    )
  (sploch)

  (when (< frame-at (blocks 9 3))
    (with-adjustments bp-adj
      (draw! (cdr (anim.next! Abpctop)))
      )
    )

  (when (within? frame-at (blocks 9 10) (blocks 9 14))
    (with-adjustments bp-adj
      (draw! (cdr (anim.next! Abpc)))
      )
    )
  ))

(define b10mesh (mesher (make-proto
    '(obj-file
      "content/graffitis/layer20.obj")
    '(img-file
      "content/graffitis/layer20.png")
    '(draw-settings
      (#t #f #f))
    '(transform
      (160 190 0 45 30))
    `(moves
      (
        ((0 1) ,(rads 80) .75)
        ((4 5) ,(rads 85) 1)
        ((12 13) ,(rads 90) .75)
        ((16 17) ,(rads 90) 1)
        ((24 25) ,(rads 90) .5)
      ))
  )))

(define stk2 (make-stacker `#(
  ,(cdr (vector-ref static-graffiti 1))
  ,(cdr (vector-ref static-graffiti 4))
  ,(cdr (vector-ref static-graffiti 0))
  ,(cdr (vector-ref static-graffiti 2))
  ) 10
  ))

(define b10g (cdr (vector-ref static-graffiti 7)))

(define b10t 0)

(at (blocks 10) (lambda ()
  (when is-first-frame
    (anim.reset! Abpctop)
    (set! b10t 0)
    )

  (stk2 'update)

  (bg-fill! '(0))

  (set-shader!)
  (set-color! 1)
  (draw! (cdr (anim.next! Aecoli)))
  (wvs)

  #|
  (with-adjustments poli-adj
    (draw! (cdr (anim.next! Ablob-poke2)))
    )
  |#

  (anim.next! Abpjump)

  (with-adjustments bp-adj
    ; (draw! (cdr (anim.peek Abpjump)) -150 0)
    )

  (set-shader!)
  (set-color! 1)
  (for 0 6 1 (lambda (i)
    (draw! (stk2) 350 (*+ i 53 150) (rads 270) 1.5)
    ))

  (with-adjustments bp-adj
    (draw! (cdr (anim.peek Abpjump)) 170 0)
    )

  (set-shader!)
  (set-color! 0 0.6)
  (rect! 0 0 (/ window-width 2) 480)

  (b10mesh 'draw)
  (b10mesh b10t)

  (+=! b10t fps)
  ))

(loop!
  (blocks 7)
  (blocks 11))

; )

; =============================
; === block 11 change

; todo jitter when she pokes you

(load-once Vstrep
  (load-frames "content/videos/strep" 20 300))
(load-once Vhidef
  (load-frames "content/videos/hidef" 1 300))

(define Ahidef (make-anim Vhidef))
(define Astrep (make-anim Vstrep))

(define color-spin (make-program
  (make-frag-string
  "
  uniform float time;

  vec4 effect() {
    vec4 ocol = texture2D(tex, uv_coord);
    float val = 0.21 * ocol.r + 0.71 * ocol.g + 0.08 * ocol.b;
    vec3 colhel = cubehelix(val, fract(time) * 3, 8, 1, 1.5);
    return vec4(colhel.rgb, ocol.a);
  }
  "
  )))

(define cs-tloc (safe-get-uniform-location color-spin "time"))
(define cs-t 0)

(define (within? num min max)
  (and (>= num min)
       (< num max))
  )

(define b11bcirc (bar-circles (make-proto
  `(C ,diatoms)
  '(r 80)
  )))

(define fullchem (chemicals (make-proto
  '(ct 15)
  )))

(define (bchems)
  (bg-fill! '(0))

  (set-color! 1)
  (set-shader!)
  (push! coords-stack (make-coords -175 0 0 2))
  (fullchem)
  (pop! coords-stack)
  )

(at (blocks 11) (lambda ()
  (when is-first-frame
    (anim.reset! Apokyu 15)
    )

  (set-shader!)
  (b11bcirc)

  (when (within? (anim.at Apokyu) 5 11)
    (bchems)
    )

  (with-adjustments poli-sharp
    (draw! (cdr (anim.next! Apokyu)) 0 -30)
    )
  ))

(at (blocks 11 8) (lambda ()
  (when is-first-frame
    (anim.reset! Astrep 30)
    (set! cs-t 0)
    )

  (+=! cs-t fps)

  (for 0 4 1 (lambda (i)
    (anim.next! Astrep)))

  (set-shader! color-spin)
  (set-color! 1)
  (when cs-tloc
    (gl:uniform1f cs-tloc cs-t))

  (draw-px! (cdr (anim.peek Astrep)) 0 0 0 window-width window-height)

  (when (within? (anim.at Apokyu) 5 11)
    (bchems)
    )

  (unless (within? (anim.at Apokyu) 5 11)
  (set-shader!)
   (bdu)
   )
  (with-adjustments poli-sharp
    (draw! (cdr (anim.next! Apokyu)) 0 -30)
    )
   (bd)

  ))

(define p2s (protozoa (make-proto
  '(scl 2)
  '(prob 0.01)
  )))

(at (blocks 12) (lambda ()
  (for 0 2 1 (lambda (i)
    (anim.prev! Astrep)))

  (+=! cs-t fps)

  (set-shader! color-spin)
  (set-color! 1)
  (when cs-tloc
    (gl:uniform1f cs-tloc cs-t))
  (draw-px! (cdr (anim.peek Astrep)) 0 0 0 window-width window-height)

  (p2s)
  (set-shader!)

  (with-adjustments poli-sharp
    (draw! (cdr (anim.next! Ablob-poke2)) 0 0)
    )

  (sploch)
  ))

(define wvs (waves (make-proto
  )))

(define nbg2 (snois (make-proto
  '(num 0)
  )))

(define posteradj (adjustment (make-proto
  '(do-levels 1)
  '(brightness (1.3 1.3 1.3))
  '(posterize-amt 8)
  )))

(at (blocks 13) (lambda ()
  (with-adjustments posteradj
    (nbg2)
    )

  (when (within? (anim.at Apokyu) 5 11)
    (bchems)
    )

  (with-adjustments poli-sharp
    (draw! (cdr (anim.next! Apokyu)) 0 -30)
    )

  (when (not (within? (anim.at Apokyu) 5 11))
    (wvs)
    )
  ))

(at (blocks 14) (lambda ()
  (when is-first-frame
    (anim.reset! Abpc))
  (with-adjustments bp-adj
    (draw! (cdr (anim.next! Aecoli)))
    )
  (wvs)
  (with-adjustments bp-adj
    (draw! (cdr (anim.next! Abpc)))
    )
  ))

(loop!
  (blocks 11)
  (blocks 12)
  )

; )

; =============================
; === block 15 change

(load-once Rfarm1
  (load-frames "content/rec/farm 1" 0 300))
(load-once Rfarm2
  (load-frames "content/rec/farm alt 2" 0 200))
(load-once Rfarmtrans
  (load-frames "content/rec/farm alt t 1" 0 300))
(load-once contraptions
  (load-frames "content/contraption" 0 13))

(define Afarm1 (make-anim Rfarm1))
(define Afarm2 (make-anim Rfarm2))
(define Afarmtrans (make-anim Rfarmtrans))

(define (lilcontraption)
  (let* (
    (lc
      (lambda (n)
        (cdr (vector-ref contraptions n))))
    (Cs (make-anim `#(
      (0.3 1 0.3)
      (0.8)
      (1 0.3 0.3)
      (0.8)
      )))
    (F (make-framelimiter 5))
    (t 0)
    )
    (lambda ()
      (+=! t fps)
      (when (F)
        (anim.next! Cs)
        )

      (set-color! 1)
      (let (
        (sa (*+ (sin (*+ t 5 0)) 0.15 1))
        (sb (*+ (sin (*+ t 5 0.5)) 0.15 1))
        (sc (*+ (sin (*+ t 5 1)) 0.15 1))
        (s2 (*+ (sin (* t 2.5)) 0.15 1))
        )
        (draw! (lc 2) 50 (+ (- (* sa 40)) 40) 0 1 sa)
        (draw! (lc 2) 75 (+ (- (* sb 40)) 40) 0 1 sb)
        (draw! (lc 2) 100 (+ (- (* sc 40)) 40) 0 1 sc)
        (draw! (lc 10) (+ (- (* s2 40)) 45) 5 0 s2 1)
        )
      (apply set-color! (anim.peek Cs))
      (rect! 8 56 70 10)
      (set-color! 1)
      )))

(define lc (lilcontraption))

; with-coords with-framebuffer

(define chch (chemicals (make-proto
  '(ct 3)
  '(F 1)
  )))

(at (blocks 15) (lambda ()
  (when is-first-frame
    (anim.reset! Aecoli)
    (anim.reset! Afarm1))

  (set-shader!)
  (set-color! 1)
  (draw! (cdr (anim.next! Afarm1)))

  (set-color! 0.4 0.9 0.6 0.4)
  (push! coords-stack (make-coords -175 0 0 2))
  (chch)
  (pop! coords-stack)

  (set-color! 1)
  (push! coords-stack (make-coords 410 30 0 1.4))
  (lc)
  (pop! coords-stack)
  ))

(define dnaplan (dna-plain (make-proto
  '(c1 (0.5))
  '(c2 (0))
  '(width 80)
  '(ct 11)
  '(height 15)
  '(over 550)
  '(spd 4)
  )))

(define slow-protos (protozoa (make-proto
  '(scl 1.5)
  '(prob 0.03)
  `(C ,mold)
  `(spd (2.0 3.0))
)))

(define greentone (make-program
  (make-frag-string
    "
    uniform float time;
    vec4 effect() {
      vec4 ocol = texture2D(tex, uv_coord);

      float bw = 0.21 * ocol.r + 0.71 * ocol.g + 0.08 * ocol.b;
      float bw2 = (ocol.r + ocol.g + ocol.b) / 3;

      vec3 col = mix(
        vec3(0, 0.5, 1) ,
        vec3(0.4, 1, 0.05) ,
        bw
      );

      col *= bw;

      float scan = sin(uv_coord.y * 300 + time * 10) / 2 + 0.5;

      scan -= 0.4;
      scan = smoothstep(0.3, 0.35, scan);
      scan /= 15;

      col.g += scan;

      return vec4(col.rgb, ocol.a);
    }
    ")
  ))

(define b17_t 0)
(define b17_tloc (safe-get-uniform-location greentone "time"))

(define b17_fb (make-framebuffer window-width window-height))
(define b17_fbt (framebuffer.tex b17_fb))

(define b17_F (make-framelimiter 2))
(at (blocks 17) (lambda ()
  (when is-first-frame
    (set! b17_t 0)
    (anim.reset! Afarmtrans))

  (gl:bind-framebuffer gl:+framebuffer+ b17_fb)
  (gl:clear gl:+color-buffer-bit+)

  (set-shader!)
  (set-color! 0.7)

  (draw! (cdr (anim.next! Aecoli)))

  (slow-protos)

  (set-shader!)
  (set-color! 1.1)
  (when (b17_F)
    (anim.next! Afarmtrans))
  (draw! (cdr (anim.peek Afarmtrans)))

  (dnaplan)

  (clear-fb!)

  (+=! b17_t fps)
  (set-shader! greentone)
  (when b17_tloc
    (gl:uniform1f b17_tloc b17_t))

  (set-uv-flip! #t)
  (draw! b17_fbt)
  (set-uv-flip! #f)
  ))

(at (blocks 19) (lambda ()
  (when is-first-frame
    (anim.reset! Afarm2))

  (set-shader!)
  (set-color! 1)
  (draw! (cdr (anim.next! Afarm2)))

  (wvs)

  ; (bdu)

  (anim.next! Awt1)
  (with-adjustments poli-adj
    (draw! (cdr (anim.peek Awt1)) 100 0)
    )
  ; (bd)
  ))

(define nice-bw (make-program
  (make-frag-string
    "
    vec4 effect() {
      vec4 col = base_color * texture2D(tex, uv_coord);
      col.rgb *= vec3(0.21, 0.71, 0.08);
      float val = col.r + col.g + col.b;
      return vec4(val, val, val + col.b *2, col.a);
    }
    "
    )))

(at (blocks 20) (lambda ()
  (when is-first-frame
    (anim.reset! Ahidef)
    (anim.reset! Afarmtrans)
    )

  (set-shader!)
  (set-color! 1)
  (draw! (cdr (anim.next! Ahidef)) 0 0 0 2)

  (anim.next! Awt2)

  (set-shader!)
  (set-color! 0)
  (draw! (cdr (anim.next! Afarmtrans)))

  (set-shader! nice-bw)
  (set-color! 1.25)
  (draw! (cdr (anim.peek Awt2)) 250 120 0 0.75)
  ))

(define Fwind (wind (make-proto
  '(wind (-20 -8))
  '(fall (6 8))
  )))

(define b21_fade 0)

(at (blocks 21) (lambda ()
  (when is-first-frame
    (set! b21_fade 0)
    (anim.reset! Afarm1))

  (when (b17_F)
    (rr 'update)
    (anim.next! Afarm1))

  (when (> frame-at (blocks 22))
    (set! b21_fade (/ (- frame-at (blocks 22)) (blocks 0 12)))
    )

  (rr 'bind)
  (set-color! 1)
  (draw! (cdr (anim.next! Ajeep)))

  (clear-fb!)

  (rr 'draw)

  (set-shader!)
  (set-color! 1 (- 1 b21_fade))
  (draw! (cdr (anim.peek Afarm1)))

  (Fwind)

  (sploch2)
  ))

(loop!
  (blocks 21)
  (blocks 23)
  )

; )

; =============================
; === block 23 change

(load-once Rpourf
  (load-frames "content/rec/pour far" 0 250))
(load-once Rsleep
  (load-frames "content/rec/sleep 1" 0 400))
(load-once Rswim
  (load-frames "content/rec/swimming" 0 150))
(load-once Rjeep
  (load-frames "content/rec/jeep bg" 0 150))
(load-once Rsleeptrans
  (load-frames "content/rec/sleep trans" 0 250))

(define Apourf (make-anim Rpourf))
(define Asleep (make-anim Rsleep))
(define Aswim (make-anim Rswim))
(define Ajeep (make-anim Rjeep))
(define Asleeptrans (make-anim Rsleeptrans))

(define wvs2 (waves (make-proto
  )))

(define pa-proto (make-proto
  '(do-hsl 1)
  '(hsl-adj (0 0.9 1.3))
  ))

(define pour-adj (adjustment pa-proto))

(define bloodF (make-framelimiter 2))

(at (blocks 23) (lambda ()
  (when is-first-frame
    (anim.reset! Apourf))
  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Apourf)))
    )
  ))

(at (blocks 24) (lambda ()
  (when is-first-frame
    (bb 'reset)
    (anim.reset! Ajeep))
  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Ajeep)))
    )
  (bdu)
  ; (when (bloodF)
    (bb 'update)
    ; )
  (bb 'draw)
  ))

(at (blocks 25) (lambda ()
  (when is-first-frame
    (anim.reset! Apourf))
  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Apourf)))
    )
  (when (bloodF)
    (bb 'update)
    )
  (bb 'draw)

  (set-shader!)
  (set-color! 1)
  (sploch2)
  ))

(at (blocks 26) (lambda ()
  (when is-first-frame
    (bb 'reset)
    (anim.reset! Ajeep))

  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Ajeep)))
    )

  (bdu)
  (wvs2)
  (bd)
  ))

(at (blocks 27) (lambda ()
  (when is-first-frame
    (anim.reset! Atpour))
  (nbg2)
  (set-shader!)
  (protos)
  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Atpour)))
    )
  ))

(at (blocks 28) (lambda ()
  (when is-first-frame
    (anim.reset! Ajeep))
  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Ajeep)))
    )

  (anim.next! Abpjump)

  (wvs2)
  (with-adjustments bp-adj
    (draw! (cdr (anim.peek Abpjump)) -130 0)
    (draw! (cdr (anim.peek Abpjump)) 130 0)
    (draw! (cdr (anim.peek Abpjump)) 0 0)
    )
  ))

(define b33_r (rays (make-proto
  '(c1 (0 0.5 0))
  '(c2 (0 0 0 0))
  '(spd 0.08)
  '(ct 10)
  )))

(at (blocks 29) (lambda ()
  (when is-first-frame
    (anim.reset! Atpour))
   (nbg2)
  (set-shader!)
  (b33_r)
  (anim.next! Atpour)
  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Atpour)))
    )
  ))

(define b33_t 0)

(at (blocks 29 8) (lambda ()
  (when is-first-frame
    (set! b33_t 0)
    (bb 'reset)
    (anim.reset! Apourf))

  (+=! b33_t fps)

  (pour-adj 'bind)
  (set-shader!)
  (set-color! 1)
  (draw! (cdr (anim.next! Apourf)))

  (rr 'update)
  (rr 'bind)
  (pa-proto 'do-levels 1)
  (pa-proto 'brightness `(,(*+ (sin b33_t) 0.4 0.6) 0.2 0.2))

  (pour-adj 'draw)

  (set-shader!)
  (bd)
  (bdu)

  (pa-proto 'do-levels 0)

  (clear-fb!)
  (rr 'draw)
  ))

; ==

(at (blocks 31) (lambda ()
  (when is-first-frame
    (anim.reset! Ajeep))
  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Ajeep)))
    )
  (wvs2)
  (sploch2)
  ))

(define b35_F (make-framelimiter 2))

(define b35_bc (bar-chart (make-proto
  '(color (0))
  '(w 26)
  '(spc 3)
  '(bmax 250)
  '(jmp 15)
  '(fspd 1)
  )))

(define (b358_l)
  (set-shader!)
  (set-color! 1)
  (when (b35_F)
    (anim.next! Aswim))
  (with-adjustments pour-adj
    (draw! (cdr (anim.peek Aswim)))
    )
  (bd)
  (bdu)

  (b35_bc)
  )

(at (blocks 31 8) (lambda ()
  (when is-first-frame
    (anim.reset! Aswim))
  (b358_l)
  ))

(loop!
  (blocks 23)
  (blocks 31))

; )

; =============================
; === block 37

(define b37_fade 0)

(define overchem (chemicals (make-proto
  '(F 2)
  '(ct 15)
  )))

(at (blocks 33) (lambda ()
  (when is-first-frame
    (anim.reset! Asleep))

  (when (< frame-at (blocks 33 4))
    (set! b37_fade (/ (- frame-at (blocks 33)) (beats 4)))
    (b358_l)
    )

  (with-adjustments pour-adj
    (set-color! 1 b37_fade)
    (draw! (cdr (anim.next! Asleep)))
    )

  (set-shader!)
  (set-color! 1 (* b37_fade 0.5))
  (push! coords-stack (make-coords -175 0 0 2))
  (overchem)
  (pop! coords-stack)
  ))

; this is ok ^
(define b41_fade 0)
(define b41_F (make-framelimiter 2))

(at (blocks 35) (lambda ()
  (when is-first-frame
    (set! b41_fade 0)
    (bb 'reset)
    (anim.reset! Asleeptrans)
    )
  (with-adjustments pour-adj
    (draw! (cdr (anim.next! Asleep)))
    )

  (when (b41_F)
    (anim.next! Asleeptrans))
  (when (within? frame-at (blocks 35 10) (blocks 36))
    (set! b41_fade (/ (- frame-at (blocks 35 10)) (beats 5)))
    )
  (set-shader! nice-bw)
  (set-color! 1 b41_fade)
  (draw! (cdr (anim.peek Asleeptrans)))

  (set-shader!)
  (set-color! 1 0.3)
  (push! coords-stack (make-coords -175 0 0 2))
  (overchem)
  (pop! coords-stack)

  (bb 'update)
  (bb 'draw)
  ))

; xxx ploi on top
(at (blocks 36) (lambda ()
  (when is-first-frame
    ; (anim.reset! Asleeptrans)
    (set! b41_fade 0)
    )
  (when (b41_F)
    (anim.next! Asleeptrans))

  (when (> frame-at (blocks 36 12))
    (set! b41_fade (/ (- frame-at (blocks 36 12)) (beats 24)))
    )

  (bg-fill! '(0.9 0.05 0.05))

  (set-shader! nice-bw)
  (set-color! 1 (- 1 b41_fade))
  (draw! (cdr (anim.peek Asleeptrans)))

  (set-shader!)
  (set-color! 0 (* (- 1 b41_fade) 0.8))
  (push! coords-stack (make-coords -175 0 0 2))
  (overchem)
  (pop! coords-stack)

  (set-shader!)

  (bd)
  (sploch)

  (bb 'update)
  (bb 'draw)

  (set-shader!)
  (bdu)
  ))

(loop!
  (blocks 35)
  (blocks 39 12)
  )
