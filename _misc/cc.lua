-- TODO noise gen, sinegen
--      print OK on load
--      randin bias
--
--      technical looking drafting font
--      color bars

local gfx = love.graphics
local rand = love.math.random

function table.reverse(t)
  for i = 1, math.floor(#t / 2) do
    local tmp = t[i]
    t[i] = t[#t - i + 1]
    t[#t - i + 1] = tmp
  end
end

function table.copy(t)
  local ret = {}
  for i = 1, # t do
    ret[i] = t[i]
  end
  return ret
end

local function rng(min, max, typ)
  if not min and not max and not typ then
    return rng(0, 1, "f")
  elseif min and not max and not typ then
    return rng(0, min, "i")
  elseif min and max and not typ then
    return rng(min, max, "i")
  end

  local ret = {}
  ret.min = min
  ret.max = max
  ret.range = max - min

  if typ == "f" then
    ret.__call = function (self)
      return rand() * self.range + self.min
    end
  else
    ret.__call = function (self)
      return rand(self.min, self.max)
    end
  end

  return setmetatable(ret, ret)
end

local function rpg(percent)
  local ret = {}
  ret.__call = function ()
    return rand() < percent
  end
  ret.prob = percent
  ret.rprob = 1 - percent
  return setmetatable(ret, ret)
end

-- TODO
--   take rng as argument instead
local function framelimiter(every, every2)
  local acc = every
  if not every2 then
    return function (ct)
      acc = acc - (ct or 1)
      if acc < 0 then
        acc = every
        return true
      else
        return false
      end
    end
  else
    return function(ct)
      acc = acc - (ct or 1)
      if acc < 0 then
        acc = rand(every, every2)
        return true
      else
        return false
      end
    end
  end
end

local function rand_in(tbl_or_str, bias)
  if not tbl_or_str then
    error("rand in got nil")
    return nil
  end
  if not bias then
    if type(tbl_or_str) == "table" then
      local t = tbl_or_str
      return t[rand(1, #t)]
    else
      local s = tbl_or_str
      local at = rand(1, #s)
      return s:sub(at, at)
    end
  else
    local t = tbl_or_str
    local b_ct = 0
    for i = 1, #bias do
      b_ct = b_ct + bias[i]
    end

    local at = rand(1, b_ct)

    local b = 0
    for i = 1, #bias do
      b = b + bias[i]
      if b > at then
        return t[at]
      end
    end
    return t[#t]
  end
end

local function init_table(ct, to_fn)
  to_fn = to_fn or function () return true end
  local t = {}
  for i = 1, ct do
    t[i] = to_fn(i)
  end
  return t
end

local function thunk(val)
  return function ()
    return val
  end
end

local function clamp(val, min, max)
  if val < min then
    return min
  elseif val > max then
    return max
  else
    return val
  end
end

-- wrap doesnt work if val is very far outside of min and max
local function wrap(val, min, max)
  if val < min then
    return max - (min - val)
  elseif val > max then
    return min + (val - max)
  else
    return val
  end
end

local function point(x, y)
  return { x = x, y = y }
end

local function offset_point(pt, x, y)
  return point(pt.x + x, pt.y + y)
end

local function point_on_circle(r, angle)
  return point(r * math.cos(angle), r * math.sin(angle))
end

--

local function color(r, g, b, a)
  return { r = r, g = g, b = b, a = a or 255}
end

local function set_color(c)
  gfx.setColor(c.r, c.g, c.b, c.a)
end

local function mix_color(a, b, amt)
  amt = amt or 0.5
  local amt1 = 1 - amt
  return color(a.r * amt1 + b.r * amt ,
               a.g * amt1 + b.g * amt ,
               a.b * amt1 + b.b * amt ,
               a.a * amt1 + b.a * amt)
end

local good_colors = {
  color( 180, 180,  10) ,
  color(  22,  22,  22) ,
  color(  44,  44,  44) ,
  color(  30, 180, 120) ,
  color( 180,  40,  40) ,
  color(  80,  60, 180) ,
}

local bodily = {
  color( 240,  30,   0) ,
  color( 150,   0,  80) ,
  color( 230,  20,  20) ,
  color(  68,  32,  20) ,
  color(  15,  15,  15) ,
  -- color(  81,  25,   8} ,
  -- color( 200,   0,   0} ,
}

local diatoms = {
  color(160, 160, 170) ,
  color(125, 125, 125) ,
  color(80, 145, 20) ,
  color(120, 145, 20) ,
}

local redblack = {
  color(200, 45, 0) ,
  color(10, 10, 10) ,
  color(5, 5, 5) ,
}

local mold = {
  color(125,  60, 125) ,
  color( 60, 200, 125) ,
  color( 60, 125,  60) ,
  color(125, 125, 125) ,
}

local C_white = color(255, 255, 255, 255)
local C_black = color(  0,   0,   0, 255)

--

local bacterias = {}

local function load_bacterias()
  if #bacterias ~= 0 then
    return
  end
  local dir = "content/bacterias/"

  local files = love.filesystem.getDirectoryItems(dir)

  for i = 1, #files do
    table.insert(bacterias, gfx.newImage(dir .. files[i]))
  end
end

--

local function shader_safe_set(s, name, ...)
  if s:getExternVariable(name) then
    s:send(name, ...)
  else
    -- print(name, "not found")
  end
end

--

-- bar charts
scenes[0x01] = function ()
  local F = framelimiter(5)
  local P_move = rpg(0.4)
  local R_jump = rng(-24, 24)

  local w = 110
  local spc = 10
  local c_sz = 6

  cc.set[0x01] = function ()
    w = 40
    spc = 10
  end

  local min = 10
  local max = 300
  local dead = 0

  local heights
  function cc.init()
    heights = init_table(cc.width / (w + spc), thunk(max / 2))
  end

  function cc.update(dt)
    if F() then
      for i = 1, #heights do
        if P_move() then
          local r = R_jump()
          if math.abs(r) < dead then
            r = 0
          end
          heights[i] = clamp(heights[i] + r, min, max)
        end
      end
    end
  end

  function cc.draw()
    set_color(C_white)
    for i = 0, #heights - 1 do
      local y = heights[i + 1]

      -- rect
      gfx.rectangle("line", spc + i * (w + spc), 0, w, y)

      -- line
      local mx = spc + (i + 0.5) * w + i * spc
      local nexty = heights[i + 2]

      if nexty then
          local nextmx = spc + (i + 1.5) * w + (i+1) * spc
          gfx.line(mx, y, nextmx, nexty)
      end

      -- circ
      set_color(C_black)
      gfx.circle("fill", mx, y, c_sz, 8)
      set_color(C_white)
      gfx.circle("line", mx, y, c_sz, 8)
    end

    -- cutoff bottom
    set_color(C_black)
    gfx.rectangle("fill", 0, 0, 800, 1)
  end
end

-- dna
scenes[0x02] = function ()
  local b_ct = 25
  local b_ht = 650

  local width = 60
  local r_spd = 2.5
  local v_spd = 160
  local phs_offset = 135
  local phs_ct = 2

  local draw_half = true

  local types = {
    "AT" ,
    "GC" ,
  }

  --

  local phs_between = (360 / b_ct) * phs_ct
  local b_spc = b_ht / b_ct

  local function new_bar(typ, vpos)
    return { typ = typ, vpos = vpos }
  end

  local bars = init_table(b_ct, function (i)
      return new_bar(rand_in(types), b_ht - (i - 1) * b_spc)
    end)

  table.reverse(bars)

  local g_phs = 0
  function cc.update(dt)
    dt = 0.015
    g_phs = g_phs + r_spd

    for i = 1, #bars do
      local bar = bars[i]
      bar.vpos = bar.vpos - v_spd * dt
      if bar.vpos < 0 then
        bar.vpos = b_ht + bar.vpos
        bar.typ = rand_in(types)
      end
    end
  end

  function cc.draw()
    set_color(C_white)

    for i = 1, #bars do
      local bar = bars[i]
      local x1 = math.sin(math.rad(g_phs + i * phs_between))
      local x2 = math.sin(math.rad(g_phs + i * phs_between + phs_offset))
      x1 = x1 * width + width + 20
      x2 = x2 * width + width + 20
      local y = bar.vpos

      gfx.line(x1, y - 3, x2, y - 3)
      gfx.line(x1, y + 3, x2, y + 3)
      if draw_half then
        local cx = (x1 + x2) / 2
        gfx.rectangle("fill", cx, y - 3, x1 - cx, 6)
      end

      local txt
      if x1 > x2 then
        txt = string.reverse(bar.typ)
      else
        txt = bar.typ
      end

      gfx.print(txt:sub(1,1), width * 2 + 40, y - 5, 0, 2)
      gfx.print(txt:sub(2,2), width * 2 + 40 + 12, y - 5, 0, 2)

      set_color(C_black)
      gfx.circle("fill", x1, y, 5)
      gfx.circle("fill", x2, y, 5)

      set_color(C_white)
      gfx.circle("line", x1, y, 5)
      gfx.circle("line", x2, y, 5)
    end
  end
end

-- chemicals
-- todo up speed down speed
scenes[0x03] = function ()
  local R_str_len = rng(3, 9)
  local chain_ct = 30
  local v_spc = 24
  local hang = 3
  local hard_max = 15
  local base_x = 60
  local F = framelimiter(3)

  --

  local function gly(next_prob, offset, must_have_next)
    return { offset = offset or 0, next_prob = next_prob , must_have_next = must_have_next }
  end

  local starts = "abcj"
  local c_glyphs = {
    a = gly("ccddddegg") ,
    b = gly("ppk") ,
    c = gly("aaddddeggkk") ,
    d = gly("aabbccdjj") ,
    e = gly("") ,
    g = gly("gggggieeek") ,
    i = gly("iiiiigeeek", 0, true) ,
    j = gly("lnmk", 7) ,
    k = gly("c", 0, true) ,
    l = gly("acj", 0, true) ,
    m = gly("") ,
    n = gly("acccj", 0, true) ,
    p = gly("ccb", 0, true) ,
  }

  for name, glyph in pairs(c_glyphs) do
    glyph.image = gfx.newImage("content/chems/" .. name .. ".png")
  end

  local function new_chain(str, x, y)
    return { x = x, y = y, str = str, at = 0, go_up = true }
  end

  local function random_str(curr_char)
    local len = R_str_len()
    local str = curr_char
    local i = 1
    while true do
      local glyph = c_glyphs[curr_char]

      if   i > hard_max
        or not glyph.must_have_next and i > len
        or #glyph.next_prob == 0 then
        break
      end

      local ch = rand_in(glyph.next_prob)
      str = str .. ch
      curr_char = ch

      i = i + 1
    end

    return str
  end

  local function rx()
    return rand(0, 10) * 30
  end

  local chains = init_table(chain_ct, function (i)
      return new_chain(random_str(rand_in(starts)), rx(), (i - 1) * v_spc)
    end)

  function cc.update(dt)
    if F() then
      for i = 1, #chains do
        c = chains[i]
        if c.go_up then
          c.at = c.at + 1
          if c.at > #c.str + hang then
            c.go_up = false
          end
        else
          c.at = c.at - 1
          if c.at < 0 then
            c.str = random_str(rand_in(starts))
            c.x = rx()
            c.go_up = true
            c.at = 0
          end
        end
      end
    end
  end

  function cc.draw()
    set_color(C_white)
    for i = 1, #chains do
      c = chains[i]
      local x = c.x + base_x
      local str = c.str:sub(1, c.at)
      for i = 1, #str do
        local ch = str:sub(i, i)
        local glyph = c_glyphs[ch]
        gfx.draw(glyph.image, x, c.y)
        x = x + glyph.image:getWidth() - glyph.offset - 1
      end
    end
  end
end

--rays
scenes[0x04] = function ()
  local spd = 0.004
  local ray_ct = 6

  cc.set[0x01] = function ()
    spd = 0.002
    ray_ct = 3
  end

  cc.set[0x02] = function ()
    spd = 0.005
    ray_ct = 500
  end

  local total_rays, phase, phs_offset, sz
  function cc.init()
    total_rays = ray_ct * 2
    phase = 0
    phs_offset = (math.pi * 2) / total_rays
    sz = cc.radius + 2
  end

  function cc.update(dt)
    phase = phase + spd
  end

  function cc.draw()
    local p = phase
    for i = 1, ray_ct do
      set_color(C_white)
      gfx.arc("fill", cc.cx, cc.cy, sz, p, p + phs_offset)
      p = p + phs_offset

      set_color(C_black)
      gfx.arc("fill", cc.cx, cc.cy, sz, p, p + phs_offset)
      p = p + phs_offset
    end
  end
end

-- color bars
-- switch min max to amplitude offset
scenes[0x05] = function ()
  local n_spd = 0.01
  local n_diff = 0.1
  local min = 3
  local max = 50

  local R_width = rng(0.25, 1.05, "f")

  local C = good_colors

  cc.set[0x01] = function ()
    n_spd = 0.01
    n_diff = 0.1
    min = 3
    max = 50

    R_width = rng(1, 1)
    C = diatoms
  end

  cc.set[0x02] = function ()
    n_spd = 0.02
    n_diff = 0.1
    min = 10
    max = 200

    R_width = rng(0.25, 1.05, "f")
    C = bodily
  end

  local function bar(color, w_mult)
    return { color = color, w_mult = w_mult}
  end

  local bars, range, noisex, bar_ct
  function cc.init()
    range = max - min
    noisex = rand(0, 100)
    bar_ct = math.floor(cc.width / (min * R_width.min)) + 4

    bars = init_table(bar_ct, function (i)
      return bar(0, 0)
    end)

    for i = 1, #bars do
      bars[i].color = rand_in(C)
      bars[i].w_mult = R_width()
    end
  end

  function cc.update(dt)
    noisex = noisex + n_spd
  end

  function cc.draw()
    -- find center
    local n_idx = noisex
    local xo = 0
    for i = 1, math.floor(#bars / 3) do
      local w = love.math.noise(n_idx) * range + min
      w = w * bars[i].w_mult
      xo = xo + w
      n_idx = n_idx + n_diff * 1.01
    end

    local n_idx = noisex
    local x = 0
    for i = 1, #bars do
      local w = love.math.noise(n_idx) * range + min
      w = w * bars[i].w_mult
      set_color(bars[i].color)
      gfx.rectangle("fill", x - xo + cc.width / 3, 0, w, cc.height)
      n_idx = n_idx + n_diff
      x = x + w
    end
  end
end

local function draw_plus(pt, size)
  size = size or 5
  local x, y = pt.x, pt.y
  gfx.line(x - size, y, x + size, y)
  gfx.line(x, y - size, x, y + size)
end

-- plus
scenes[0x06] = function ()
  load_bacterias()
  local sz = 7
  local n1_spd = 0.02
  local n2_spd = 0.005
  local n1_scl = 20
  local n2_scl = 200
  local n1_offset = 0.15
  local n2_offset = 0.04

  local R_xmov = rng(-10, 10)
  local R_ymov = rng(-10, 10)

  local spc = 80

  local C = good_colors

  cc.set[0x01] =  function ()
    n1_spd = 0.02
    n2_spd = 0.01
    n1_scl = 20
    n2_scl = 200
    n1_offset = 0.15
    n2_offset = 0.04
    R_xmov = thunk(0)
    R_ymov = thunk(0)
    spc = 120
  end

  local points = {}
  local n1, n2
  function cc.init()
    local ct = 20
    local x, y = 0, 0
    for x = 0, cc.width, spc do
      for y = 0, cc.height * 3, spc do
        local p = point(x + R_xmov(), y + R_ymov() - spc)
        p.img = rand_in(bacterias)
        p.r = rand(0, 360)
        p.c = mix_color(rand_in(C), C_white, 0.5)
        table.insert(points, p)
      end
    end

    n1, n2 = rand(0, 100), rand(0, 100)
  end

  function cc.update(dt)
    n1 = n1 + n1_spd
    n2 = n2 + n2_spd
  end

  function cc.draw()
    set_color(C_white)
    gfx.setLineWidth(2)

    local n1i = n1
    local n2i = n2
    for i = 1, #points do
      local nn1 = love.math.noise(n1i) * n1_scl
      local nn2 = love.math.noise(n2i) * n2_scl
      -- draw_plus(offset_point(points[i], nn1, nn2), sz)
      local p = points[i]
      set_color(p.c)
      gfx.draw(p.img, p.x + nn1, p.y + nn2, math.rad(p.r))

      n1i = n1i + n1_offset
      n2i = n2i + n2_offset
    end
  end
end

-- plus good
_ = function ()
  local sz = 5
  local nx_spd = 0.005
  local ny_spd = 0.005
  local nx_scl = 50
  local ny_scl = 50
  local nx_offset = 0.04
  local ny_offset = 0.04
  local ct = 25

  local points = {}
  local nx, ny
  function cc.init()
    local stagger = false
    for j = 0, ct - 1 do
      for i = 0, ct - 1 do
        local m = 80
        table.insert(points, point(i * m + (stagger and 0 or m / 2) - 400, j * m * .5 - 120))
      end
      stagger = not stagger
    end

    nx, ny = rand(0, 100), rand(0, 100)
  end

  function cc.update(dt)
    nx = nx + nx_spd
    ny = ny + ny_spd
  end

  function cc.draw()
    set_color(C_white)
    gfx.setLineWidth(2)

    local nxi = nx
    local nyi = ny
    local xos = {}
    local yos = {}
    local nnx, nny = 0, 0

    local acc = 0

    for i = 0, ct - 1 do
      nnx = nnx * 0.9 + love.math.noise(nxi) * nx_scl - acc
      nny = nny * 0.3 + love.math.noise(nyi) * ny_scl
      table.insert(xos, nnx)
      table.insert(yos, nny)
      nxi = nxi + nx_offset
      nyi = nyi + ny_offset
      acc = acc + 1
    end

    local at = 1
    for i = 1, ct do
      for j = 1, ct do
        draw_plus(offset_point(points[at], xos[i], yos[j]), sz)
        at = at + 1
      end
    end
  end
end

-- anenomie
scenes[0x07] = function ()
  local c_sz = 4
  local r_spc = 5
  local mult = 2
  local spc = 30
  local C_mid = rand_in(diatoms)
  local C_ring = rand_in(bodily)
  local ring_w = 1

  cc.set[0x01] = function ()
    c_sz = 14
    r_spc = 0
    mult = 2
    -- C_ring = C_mid
    spc = 28
    ring_w = 3
  end

  cc.set[0x02] = function ()
    C_mid = nil
    C_ring = rand_in(diatoms)
    c_sz = 3
    mult = 2
    spc = 30
  end

  local function ring(phs, r, ct)
    return { phs = phs, ct = ct, r = r}
  end

  local function draw_ring(ring)
    local spc = 360 / ring.ct
    for i = 1, ring.ct do
      local p = point_on_circle(ring.r, math.rad(i * spc + math.sin(ring.phs) * 30 + ring.phs * 15))
      local op = offset_point(p, cc.cx, cc.cy)
      if C_mid then
        set_color(C_mid)
        gfx.circle("fill", op.x, op.y, c_sz, 15)
        set_color(C_ring)
        gfx.setLineWidth(ring_w)
        -- gfx.circle("line", op.x, op.y, c_sz + spc)
        gfx.circle("line", op.x, op.y, c_sz + r_spc, 15)
      else
        set_color(C_ring)
        gfx.setLineWidth(ring_w)
        gfx.circle("line", op.x, op.y, c_sz, 15)
      end
    end
  end

  local ct, rings
  function cc.init()
    ct = cc.radius / spc + 4
    rings = {}
    for i = 1, ct do
      rings[i] = ring(0, (i - 1) * spc, (i - 1) * mult)
    end
  end

  local s_phs = 0
  function cc.update(dt)
    for i = 1, ct do
      local ring = rings[i]
      -- ring.phs = ring.phs + math.sin(i * 0.01 * s_phs)
      ring.phs = ring.phs + i * 0.001
    end
    s_phs = s_phs + 0.04
  end

  function cc.draw()
    gfx.setLineWidth(1)
    gfx.setColor(255, 255, 255, 255)
    for i = 1, #rings do
      draw_ring(rings[i])
    end
  end
end

-- color circles
scenes[0x08] = function ()
  local R_small = rng(5, 15)
  local R_big = rng(50, 60)
  local R_choose = rng(0, 2)
  local F = framelimiter(5, 10)

  local s_spd = 0.025
  local s_diff = 0.1
  local s_mult = 13

  local P_big = rpg(0.15)

  local colors = bodily

  cc.set[0x01] = function ()
    R_small = rng(5, 15)
    R_big = rng(30, 49)
    R_choose = rng(0, 2)
    F = framelimiter(5, 10)

    s_spd = 0.008
    s_diff = 0.1
    s_mult = 18

    P_big = rpg(0.6)

    local c = table.copy(redblack)
    c[1] = color(75, 200, 125)
    colors = c
  end

  cc.set[0x02] = function ()
    R_small = rng(5, 10)
    R_big = rng(40, 60)
    R_choose = rng(3, 5)
    F = framelimiter(10, 25)

    s_spd = 0.025
    s_diff = 0.1
    s_mult = 10
    P_big = rpg(0.9)

    colors = diatoms
  end

  local function rw()
    local is_big = P_big()
    return is_big and R_big() or R_small()
  end

  local function circ(offset, color)
    return { offset = offset, color = color }
  end

  local ct, circs, acc
  function cc.init()
    ct = math.floor(cc.radius
           / (R_small.min * P_big.rprob
             + R_big.min * P_big.prob))
         + 5

    circs = init_table(ct, function (i)
      return circ(rw(), rand_in(colors))
    end)

    acc = 0
  end

  function cc.update(dt)
    if F() then
      for i = 1, R_choose() do
        local c = rand_in(circs)
        c.offsets = rw()
        c.color = rand_in(colors)
      end
    end
    acc = acc + s_spd
  end

  function cc.draw()
    local r = 0
    local rs = {}
    for i = 1, #circs do
      local o = circs[i].offset
      r = r + o
      table.insert(rs, #rs + 1, r)
    end

    local sacc = acc
    for i = 1, #circs do
      set_color(circs[i].color)
      gfx.circle("fill", cc.cx, cc.cy, rs[#rs - i + 1])
      -- gfx.circle("fill", cc.cx, cc.cy, rs[#rs - i + 1] + math.sin(sacc) * s_mult)
      sacc = sacc + s_diff
    end
  end
end

function gfx.loadObj(filename)
  local obj = love.filesystem.read(filename)

  local vs = {}
  local uvs = {}
  local fs = {}
  local vmap = {}

  obj:gsub("v (%-?%d+%.%d+) (%-?%d+%.%d+)[^\n]*\n",
    function (xstr, ystr)
      table.insert(vs, { tonumber(xstr), tonumber(ystr) })
    end)

  obj:gsub("vt (%-?%d+%.%d+) (%-?%d+%.%d+)[^\n]*\n",
    function (xstr, ystr)
      table.insert(uvs, { tonumber(xstr), tonumber(ystr) })
    end)

  obj:gsub("f (%d+)/(%d+) (%d+)/(%d+) (%d+)/(%d+)[^\n]*\n",
    function(pt1mesh, pt1uv, pt2mesh, pt2uv, pt3mesh, pt3uv)
      local t = { pt1mesh, pt1uv ,
                  pt2mesh, pt2uv ,
                  pt3mesh, pt3uv }
      for i = 1, #t, 2  do
        local vi = tonumber(t[i])
        local uvi = tonumber(t[i + 1])
        local uv = uvs[uvi]
        if #vs[vi] ~= 4 then
          vs[vi][2] = -vs[vi][2]
          table.insert(vs[vi], uv[1])
          table.insert(vs[vi], 1 - uv[2])
        end
        table.insert(vmap, vi)
      end
    end)

  local ret = gfx.newMesh(vs)
  ret:setVertexMap(vmap)
  ret:setDrawMode("triangles")

  return ret
end

local function draw_mesh(m, x, y, sx, sy)
  x = x or 0
  y = y or 0
  sx = sx or 1
  sy = sy or sx

  local vmap = m:getVertexMap()
  local xs = {}
  local ys = {}
  for i = 1, m:getVertexCount() do
    local x, y = m:getVertex(i)
    table.insert(xs, x)
    table.insert(ys, y)
  end

  local ls = gfx.getLineStyle()
  gfx.setLineStyle("rough")

  for i = 1, #vmap, 3 do
    local tri = { vmap[i], vmap[i + 1], vmap[i + 2] }
    local pts = { 0, 0, 0, 0, 0, 0 }
    for j = 1, #tri do
      local idx = tri[j]
      local xc = xs[idx] * sx + x
      local yc = ys[idx] * sy + y
      gfx.print(idx, xc + 3, yc - 9)
      pts[j * 2 - 1] = xc
      pts[j * 2] = yc
    end
    gfx.polygon("line", pts)
  end

  gfx.setLineStyle(ls)
end

scenes[0x09] = function ()
  -- local mesh = gfx.loadObj("content/untitled.obj")
  local mesh = gfx.loadObj("content/cutouttest.obj")
  local it = gfx.newImage("content/pg8 042.png")
  mesh:setTexture(it)

  local function mov(num, spd, dt)
    local x, y, ux, uy = mesh:getVertex(num)
    mesh:setVertex(num, x + (rand() * 3 - 1.5) * dt, y + spd * dt, ux, uy)
  end

  function cc.update(dt)
    local t = {
      1, 2,
      11, 12,
      21, 22,

      5, 6,
      15, 16,
      25, 26,

      3, 4,
      13, 14,
      23, 24,
    }
    local s = {
      4, 4,
      5, 5,
      4, 4,

      3, 3,
      3, 3,
      2, 2,

      1, 1,
      1, 1,
      1, 1,
    }
    for i = 1, #t do
      mov(t[i], s[i] / 3, dt)
    end
  end

  function cc.draw()
    set_color(C_white)
    gfx.draw(mesh, cc.cx, 200, 0, 20)
    draw_mesh(mesh, cc.cx, 200, 20)
  end
end

local function nos_bg()
  local imgd = love.image.newImageData(cc.width, cc.height)
  local ox = rand(0, 100)
  local oy = rand(0, 100)
  for x = 0, cc.width - 1 do
    for y = 0, cc.height - 1 do
      local n = love.math.noise(x/ 500 + ox, y/ 500 + oy) * 2 / 3
      n = n + love.math.noise(x/ 150 + ox, y/ 150 + oy) / 3
      n = n * 127 + 127
      imgd:setPixel(x, y, 120, 120, 250, n)
    end
  end

  return gfx.newImage(imgd)
end

-- local i = nos_bg()

-- waves
scenes[0x0A] = function ()

  -- ++ = more variation
  local xr = 0.004
  local yr = 0.004

  local xs = 6
  local ys = 6

  -- local C_t = color( 40,  40, 180)
  -- local C_b = color(80, 80, 180)
  -- local C_t = color(150, 20, 120)
  local C_t = color(60, 20, 190)
  -- local C_b = color( 10, 80,  30)
  local C_b = C_black

  local prob = 0.018

  local pts = {}

  do
    local nx = rand(1, 100)
    local ny = rand(1, 100)
    local nxi = nx
    local nyi = ny
    local x = 0
    local y = 0
    while y < cc.height do
      nxi = nx
      x = 0
      while x < cc.width do
        if rand() < love.math.noise(nxi, nyi) * prob then
          local p = point(x, y)
          p.r = rand(0, 360)
          table.insert(pts, p)
        end
        nxi = nxi + xr
        x = x + xs
      end
      nyi = nyi + yr
      y = y + ys
    end
  end

  local mesh = gfx.newMesh {
    {0, 0, 0, 0, C_t.r, C_t.g, C_t.b, C_t.a } ,
    {cc.width, 0, 0, 0, C_t.r, C_t.g, C_t.b, C_t.a } ,
    {cc.width, cc.height, 0, 0, C_b.r, C_b.g, C_b.b, C_b.a } ,
    {0, cc.height, 0, 0, C_b.r, C_b.g, C_b.b, C_b.a } ,
  }

  local nx = rand(1, 100)
  local ny = rand(1, 100)
  local nx_spd = 0.005
  local ny_spd = 0.005
  local nx_scl = 400
  local ny_scl = 400
  local mx = 80
  local my = 120

  local star = gfx.newImage("content/pixstr.png")
  star:setFilter("nearest", "nearest")

  local function draw_star(p, r)
    gfx.draw(star, math.floor(p.x - 13), math.floor(p.y - 10), math.deg(r), 2)
  end

  function cc.update(dt)
    nx = nx + nx_spd
    ny = ny + ny_spd
  end

  function cc.draw()
    set_color(C_white)
    -- gfx.draw(mesh)

    -- gfx.draw(i)

    local xexp = 0
    local yexp = 0

    for i = 1, #pts do
      local p = pts[i]
      local xmov = love.math.noise(p.x / nx_scl + nx)
      local ymov = love.math.noise(p.y / ny_scl + ny)
      draw_star(offset_point(p
        , xmov * mx - mx / 2
        , ymov * my * (yexp) - my / 2), p.r)
      -- draw_plus(offset_point(p, xmov * mx - mx/2, ymov * my - my/2), 12)
      xexp = xexp + xmov / 4
      yexp = yexp + ymov / 4
    end

  end
end

-- bar circles
scenes[0x0b] = function ()
  local F = framelimiter(10)

  local r  = 50
  local C = good_colors

  cc.set[0x01] = function ()
    C = bodily
  end

  cc.set[0x02] = function ()
    C = diatoms
  end

  cc.set[0x03] = function ()
    C = redblack
    r = 70
  end

  local function gen()
    return rand_in(C)
  end

  local queue = {}

  function cc.init()
    local ct = cc.radius / r + 2
    for i = 1, ct do
      table.insert(queue, 0, gen())
    end
  end

  function cc.update(dt)
    if F() then
      table.insert(queue, 0, gen())
      table.remove(queue)
    end
  end

  function cc.draw()
    for i = #queue, 1, -1 do
      set_color(queue[i])
      gfx.circle("fill", cc.cx, cc.cy, i * r)
    end
  end
end

-- ruler
scenes[0x0c] = function ()
  local f = gfx.newFont(20)
  -- spd must go into spc evenly
  local spd = 4
  local spc = 20

  local small = 8
  local mid = 20
  local big = 30
  local div = 12

  local at = 0
  local ct = 0
  function cc.update(dt)
    at = at - spd
    if at <= -spc then
      at = 0
      ct = ct + 1
    end
  end

  function cc.draw()
    gfx.setFont(f)
    set_color(C_white)
    gfx.setLineWidth(2)
    local lct = ct
    for x = 0, cc.width, spc do
      local yt, yb
      if lct % div == 0 then
        yt = cc.cy - big
        yb = cc.cy + big
        gfx.print(lct / div * 3, x + at - 6, cc.cy - big - 25, 0, 1)
      elseif lct % (div/3) == 0 then
        yt = cc.cy - mid
        yb = cc.cy + mid
      else
        yt = cc.cy - small
        yb = cc.cy + small
      end
      gfx.line(x + at, yt, x + at, yb)
      lct = lct + 1
    end
  end
end

-- protozoa
scenes[0x0d] = function ()
  local prob = 0.02
  local xr = 0.004
  local yr = 0.004
  local xs = 10
  local ys = 10

  local p_rate = rng(1, 3, "f")
  local mov = rng(1,15, "f")
  local rev_p = rpg(0.5)

  local C = bodily

  cc.set[0x01] = function ()
    prob = 0.4
    -- C = redblack
  end

  load_bacterias()

  local pts = {}
  function cc.init()
    local nx = rand(1, 100)
    local ny = rand(1, 100)
    local nxi = nx
    local nyi = ny
    local x = 0
    local y = 0
    while y < cc.height do
      nxi = nx
      x = 0
      while x < cc.width do
        if rand() < love.math.noise(nxi, nyi) * prob then
          local p = point(x, y)
          p.r = rand(0, 3) * 90
          p.img = rand_in(bacterias)
          -- p.phs = rand(0, 360)
          p.phs = 0
          p.rate = rev_p() and p_rate() or -p_rate()
          p.mov = mov()
          p.turn = rand(180, 270)
          p.c = mix_color(rand_in(C), C_white, 0.5)
          table.insert(pts, p)
        end
        nxi = nxi + xr
        x = x + xs
      end
      nyi = nyi + yr
      y = y + ys
    end
  end

  function cc.update()
    for i = 1, #pts do
      local p = pts[i]
      p.phs = p.phs + p.rate
    end
  end

  function cc.draw()
    set_color(C_white)
    for i = 1, #pts do
      local p = pts[i]
      local c = point_on_circle(p.mov, math.rad(p.phs))
      -- gfx.circle("fill", p.x, p.y, 2)
      set_color(p.c)
      gfx.draw(p.img, p.x + c.x, p.y + c.y, math.rad(p.r + p.phs + p.turn))
    end
  end
end

-- wind
scenes[0x0e] = function ()
  local pt_ct = rand(45, 60)
  local wind = rand(-8, 8)
  local sm_pt = rng(5, 10, "f")
  local big_pt = rng(10, 25, "f")
  local big_prob = rpg(0.7)
  local fall = rand (2, 4)
  local mult = 2
  local diff = 1

  local pts = {}
  function cc.init()
    for i = 1, pt_ct do
      local p = point(rand(cc.width), rand(cc.height))
      p.r = (big_prob() and big_pt or sm_pt)()
      p.w = p.r * diff
      p.c = mix_color(rand_in(mold), rand_in(bodily), rand() * 0.5 + 0.5)
      table.insert(pts, p)
    end
    table.sort(pts, function (a, b)
      return a.r > b.r
    end)
  end

  function cc.update(dt)
    for i = 1, #pts do
      local p = pts[i]
      p.x = p.x + 1 / p.w * wind * mult
      p.y = p.y + 1 / p.w * fall * mult
      p.x = wrap(p.x, 0 - p.r, cc.width + p.r)
      p.y = wrap(p.y, 0 - p.r, cc.height + p.r)
    end
  end

  function cc.draw()
    for i = 1, #pts do
      local p = pts[i]
      set_color(p.c)
      gfx.circle("fill", p.x, p.y, p.r)
    end
  end
end

scenes[0x0f] = function ()


  -- {{{
  local noise2d = [[
//
// GLSL textureless classic 2D noise "cnoise",
// with an RSL-style periodic variant "pnoise".
// Author:  Stefan Gustavson (stefan.gustavson@liu.se)
// Version: 2011-08-22
//
// Many thanks to Ian McEwan of Ashima Arts for the
// ideas for permutation and gradient selection.
//
// Copyright (c) 2011 Stefan Gustavson. All rights reserved.
// Distributed under the MIT license. See LICENSE file.
// https://github.com/stegu/webgl-noise
//

vec4 mod289(vec4 x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x)
{
  return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

vec2 fade(vec2 t) {
  return t*t*t*(t*(t*6.0-15.0)+10.0);
}

// Classic Perlin noise
float cnoise(vec2 P)
{
  vec4 Pi = floor(P.xyxy) + vec4(0.0, 0.0, 1.0, 1.0);
  vec4 Pf = fract(P.xyxy) - vec4(0.0, 0.0, 1.0, 1.0);
  Pi = mod289(Pi); // To avoid truncation effects in permutation
  vec4 ix = Pi.xzxz;
  vec4 iy = Pi.yyww;
  vec4 fx = Pf.xzxz;
  vec4 fy = Pf.yyww;

  vec4 i = permute(permute(ix) + iy);

  vec4 gx = fract(i * (1.0 / 41.0)) * 2.0 - 1.0 ;
  vec4 gy = abs(gx) - 0.5 ;
  vec4 tx = floor(gx + 0.5);
  gx = gx - tx;

  vec2 g00 = vec2(gx.x,gy.x);
  vec2 g10 = vec2(gx.y,gy.y);
  vec2 g01 = vec2(gx.z,gy.z);
  vec2 g11 = vec2(gx.w,gy.w);

  vec4 norm = taylorInvSqrt(vec4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
  g00 *= norm.x;  
  g01 *= norm.y;  
  g10 *= norm.z;  
  g11 *= norm.w;  

  float n00 = dot(g00, vec2(fx.x, fy.x));
  float n10 = dot(g10, vec2(fx.y, fy.y));
  float n01 = dot(g01, vec2(fx.z, fy.z));
  float n11 = dot(g11, vec2(fx.w, fy.w));

  vec2 fade_xy = fade(Pf.xy);
  vec2 n_x = mix(vec2(n00, n01), vec2(n10, n11), fade_xy.x);
  float n_xy = mix(n_x.x, n_x.y, fade_xy.y);
  return 2.3 * n_xy;
}

// Classic Perlin noise, periodic variant
float pnoise(vec2 P, vec2 rep)
{
  vec4 Pi = floor(P.xyxy) + vec4(0.0, 0.0, 1.0, 1.0);
  vec4 Pf = fract(P.xyxy) - vec4(0.0, 0.0, 1.0, 1.0);
  Pi = mod(Pi, rep.xyxy); // To create noise with explicit period
  Pi = mod289(Pi);        // To avoid truncation effects in permutation
  vec4 ix = Pi.xzxz;
  vec4 iy = Pi.yyww;
  vec4 fx = Pf.xzxz;
  vec4 fy = Pf.yyww;

  vec4 i = permute(permute(ix) + iy);

  vec4 gx = fract(i * (1.0 / 41.0)) * 2.0 - 1.0 ;
  vec4 gy = abs(gx) - 0.5 ;
  vec4 tx = floor(gx + 0.5);
  gx = gx - tx;

  vec2 g00 = vec2(gx.x,gy.x);
  vec2 g10 = vec2(gx.y,gy.y);
  vec2 g01 = vec2(gx.z,gy.z);
  vec2 g11 = vec2(gx.w,gy.w);

  vec4 norm = taylorInvSqrt(vec4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
  g00 *= norm.x;  
  g01 *= norm.y;  
  g10 *= norm.z;  
  g11 *= norm.w;  

  float n00 = dot(g00, vec2(fx.x, fy.x));
  float n10 = dot(g10, vec2(fx.y, fy.y));
  float n01 = dot(g01, vec2(fx.z, fy.z));
  float n11 = dot(g11, vec2(fx.w, fy.w));

  vec2 fade_xy = fade(Pf.xy);
  vec2 n_x = mix(vec2(n00, n01), vec2(n10, n11), fade_xy.x);
  float n_xy = mix(n_x.x, n_x.y, fade_xy.y);
  return 2.3 * n_xy;
}
  ]] -- }}}

  -- {{{
  local noise3d = [[
vec3 mod289(vec3 x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x)
{
  return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

vec3 fade(vec3 t) {
  return t*t*t*(t*(t*6.0-15.0)+10.0);
}

// Classic Perlin noise
float cnoise(vec3 P)
{
  vec3 Pi0 = floor(P); // Integer part for indexing
  vec3 Pi1 = Pi0 + vec3(1.0); // Integer part + 1
  Pi0 = mod289(Pi0);
  Pi1 = mod289(Pi1);
  vec3 Pf0 = fract(P); // Fractional part for interpolation
  vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
  vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
  vec4 iy = vec4(Pi0.yy, Pi1.yy);
  vec4 iz0 = Pi0.zzzz;
  vec4 iz1 = Pi1.zzzz;

  vec4 ixy = permute(permute(ix) + iy);
  vec4 ixy0 = permute(ixy + iz0);
  vec4 ixy1 = permute(ixy + iz1);

  vec4 gx0 = ixy0 * (1.0 / 7.0);
  vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;
  gx0 = fract(gx0);
  vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
  vec4 sz0 = step(gz0, vec4(0.0));
  gx0 -= sz0 * (step(0.0, gx0) - 0.5);
  gy0 -= sz0 * (step(0.0, gy0) - 0.5);

  vec4 gx1 = ixy1 * (1.0 / 7.0);
  vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;
  gx1 = fract(gx1);
  vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
  vec4 sz1 = step(gz1, vec4(0.0));
  gx1 -= sz1 * (step(0.0, gx1) - 0.5);
  gy1 -= sz1 * (step(0.0, gy1) - 0.5);

  vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);
  vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);
  vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);
  vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);
  vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);
  vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);
  vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);
  vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);

  vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
  g000 *= norm0.x;
  g010 *= norm0.y;
  g100 *= norm0.z;
  g110 *= norm0.w;
  vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
  g001 *= norm1.x;
  g011 *= norm1.y;
  g101 *= norm1.z;
  g111 *= norm1.w;

  float n000 = dot(g000, Pf0);
  float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
  float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
  float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
  float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
  float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
  float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
  float n111 = dot(g111, Pf1);

  vec3 fade_xyz = fade(Pf0);
  vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
  vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
  float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x); 
  return 2.2 * n_xyz;
}
  ]] -- }}}

  -- {{{
  local s = gfx.newShader [[
  extern float time;

  vec2 pt;
  vec2 center;

  const float pi = 3.1415927;

  float circ(float lenin, float mph, float div) {
    float ret = length(pt - center) + lenin;
    ret = smoothstep(0.1, 0.5, ret * (sin(time *pi * 1/div + pi * mph/3) /2 +1.2));
    ret -= 0.5;
    ret *= 2;
    ret = 0.03/abs(ret) - 0.1;
    return ret;
  }

  vec4 effect(vec4 color, Image t, vec2 txc, vec2 scc) {
    // float x = gl_FragCoord.x / 800;
    // float col = abs(sin(time));
    // return vec4(x, col, 1 - x, 1);

    float r, g, b;

    float i;
    float tm = modf(time/ 5, i);
    tm *= 3.1415927 * 2;

    r = sin(tm);
    b = sin(tm + 3.14157*2/3);
    g = sin(tm + 3.14157*2/3*2);

    vec3 rainbow = vec3(r, g, b);

    pt = gl_FragCoord.xy / 600;
    center = vec2(.666666, .5);


    float u = atan(pt.x -0.66666, pt.y - 0.5) / pi /2;
    if (u < 0) {
      u = 1 + u;
    }
    // u = sin(u * pi * 2*10 + time*5)/2+0.5;
    u = abs(modf(u *10+time+smoothstep(-0.3, 0.3, sin(time*2)*0.5), i) - 0.5) * 2;
    u *= 1-length(pt - center)*3;
    if (u < 0) {
      u = 0;
    }
    // u = smoothstep(0.1, 0.9, u);
    float rdist = circ(u, 0, 3);
    float gdist = circ(u, 2, 3);
    float bdist = circ(u, 4, 3);

    rdist = circ(u/10, 0, 3);
    bdist = circ(u/10, 2, 3);
    gdist = circ(u/10, 4, 3);
    rainbow = vec3(rdist, 0, 0);
    rainbow += vec3(gdist, 0, 0);
    rainbow += vec3(bdist, 0, 0);

    rdist = circ(u/10, 1, 3);
    bdist = circ(u/10, 3, 3);
    gdist = circ(u/10, 5, 3);
    rainbow += vec3(rdist, 0, 0);
    rainbow += vec3(gdist, 0, 0);
    rainbow += vec3(bdist, 0, 0);

    /*
    rainbow = vec3(0, rdist, 0);
    rainbow += vec3(0, gdist, 0);
    rainbow += vec3(0, bdist, 0);
    rdist = circ(u, 1, 3);
    gdist = circ(u, 3, 3);
    bdist = circ(u, 5, 3);
    rainbow += vec3(0, rdist, 0);
    rainbow += vec3(0, gdist, 0);
    rainbow += vec3(0, bdist, 0);
    rdist = circ(u, 1.5, 6);
    gdist = circ(u, 3.5, 6);
    bdist = circ(u, 5.5, 6);
    rainbow += vec3(0, rdist, 0);
    rainbow += vec3(0, gdist, 0);
    rainbow += vec3(0, bdist, 0);
    // rainbow = vec3(u, u, u);
    */

    /*
    if (tm < 0.3333) {
      g = b = 0;
      r = tm / 3 * 10;
    } else if (tm < 0.6666) {
      r = b = 0;
      g = (tm - 0.3333) / 3 * 10;
    } else {
      r = g = 0;
      b = (tm - 0.6666) / 3 * 10;
    }
    */
    // r = g = b;

    return vec4(rainbow, 1);
    // return vec4(rdist, gdist, 0, 1);
  }
  ]] -- }}}

  local s = gfx.newShader(noise3d.. [[
  extern float time;


extern float rand;

float tri(float val) {
     float i;
     val = modf(abs(val), i);
     if (mod(i, 2) == 0)
       val = (1-val);
     return val;
}

float distanceToLine(vec2 p1, vec2 p2, vec2 point) {
    float a = p1.y-p2.y;
    float b = p2.x-p1.x;
    return abs(a*point.x+b*point.y+p1.x*p2.y-p2.x*p1.y) / sqrt(a*a+b*b);
}

float DistToLine(vec2 pt1, vec2 pt2, vec2 testPt)
{
  vec2 lineDir = pt2 - pt1;
  vec2 perpDir = vec2(lineDir.y, -lineDir.x);
  vec2 dirToPt1 = pt1 - testPt;
  return abs(dot(normalize(perpDir), dirToPt1));
}

float sinbetw(float val, float min, float max) {
  return (sin(val)/2+0.5)*(max-min)+min;
}


  vec4 effect(vec4 color, Image t, vec2 txc, vec2 scc) {
    vec2 scr = gl_FragCoord.xy / 600;
    vec2 center = vec2(0.5, 0.5);
    float dist = length(center - scr);

    float st = sin(time);

    dist = abs(dist - 0.5) * 2;
    dist = step(0.01, dist);

    // txc *= vec2(sin(time*2)/2+0.5, 1);

    // txc = abs(sin(txc * 3.1415927));
    // txc = abs(sin(abs(sin((txc+time/10)*8))*8*sin(time)));

    /*
    vec2 s= vec2(time, time)/3;
    txc.x = abs(cnoise(txc + s));
    txc.y = abs(cnoise(txc + s));
    txc = smoothstep(0.3, 0.7, txc);
    // txc /=2;
    */

    /*
    float x = cnoise(vec2(txc.x, 0)*2 + time / 4)/2+0.5;
    float y = cnoise(vec2(0, txc.y)*2 + time / 4)/2+0.5;
    // float c = cnoise(txc*5+time/4)/2+0.5;
    float c = y * x;

    txc *= vec2(x, y)/10+0.9;

    if (txc.x>1) {
      txc.x = 1 - fract(txc.x);
    }
    if (txc.y>1) {
      txc.y = 1 - fract(txc.y);
    }

    // return vec4(dist, dist, dist, 1);
    // return vec4(txc.x,txc.y,0,1);
    return texture2D(t, txc);
    */

    /*
    float c = length(vec2(txc.x/3, txc.y/4)*1.2);

    // if (c > 1)
      // c = 1 - fract(c);

    // c = abs(c * 2 - 1);
    float i;
    // c = mod(c * 8 - time, i) / 2;
    // c = abs(sin(c * 3.14*2-time));

     // c = modf(abs(c*3-time), i);
     // if (mod(i, 2) == 0)
       // c = (1-c);

    c = mod(c*8-time, 1)/2+sin(time)/2+0.4;


    vec2 offset = vec2(0.15, 0.15);
    vec4 p= texture2D(t, vec2(c/2, txc.y)/4+cnoise(vec2(time/5, 0))*0.3+offset);
    return vec4(0, p.g, p.r -p.b, 1);
    float f = tri(time*2.1);
    float f2 = tri(time*3+0.5);
    // return vec4(p.r, p.r*f, p.r -p.b*f2, 1);

  */


  /*
  float c = length(.5-scr);
  c = cnoise(scr*9)/2+0.5;

  vec2 pt1 = vec2(0.2, 0.2);
  vec2 pt2 = vec2(0.5, 0.5);

  float l,m,n;
  l = abs(distance(scr, pt1));
  m = abs(distance(scr, pt2));
  n = abs(distance(pt1, pt2));

  if (l >= n
      || m>= n)
    c += 0;

  // float p = (l + m + n) * (sin(time)/18+0.5);
  float p = (l + m + n) * 0.5;

float h =  2/n * sqrt( p * ( p - l) * ( p - m) * ( p - n));

float Thickness = 0.005;

// c += mix(1.0, 0.0, smoothstep(0.5 * Thickness, 1.5 * Thickness, h));
c=h;
*/

  float c = length(.5-scr);
  // c = cnoise(scr*9)/2+0.5;

  vec2 pt1 = vec2(0.0, 0.0);
  vec2 pt2 = vec2(1.0, 1.0);

  vec2 cen = (pt1 + pt2)/sinbetw(time, 1, 4);
  vec2 dir = pt2 - pt1;
  vec2 perp = vec2(dir.y, -dir.x);

  c = smoothstep(0.001, 0.01, DistToLine(pt1, pt2, scr));
  c += smoothstep(0.1, 0.2, DistToLine(cen + perp, cen, scr));


  c = cnoise(vec3(scr*10,time/2))/2+0.5;
  c = smoothstep(0.4, 0.6, c);

  return vec4(c,c,c,1);

  }
  ]])

  local i = gfx.newImage("content/yellow.png")
  local q = gfx.newQuad(0, 0, 600, 600, i:getDimensions())

  if s:getExternVariable("rand") then
    s:send("rand", rand())
  end

  local t = 0
  function cc.update(dt)
    t = dt + t
    if s:getExternVariable("time") then
      s:send("time", t)
    end

  end

  function cc.draw()
    gfx.setShader(s)
    -- gfx.rectangle("fill", 0, 0, cc.width, cc.height)
    gfx.rectangle("fill", 0, 0, 600, 600)
    -- gfx.draw(i, q)
    gfx.setShader()
  end
end

-- lua reaction diffusion
scenes[0x10] = function ()
  local width = 150
  local height =150
  local scl = 4

  local da = .7
  local db = .2
  local feed = 0.055
  local kill = 0.062

  local r = rpg(0.8)

  local prev = {}
  local nxt = {}
  for i = 1, width do
    table.insert(prev, {})
    table.insert(nxt, {})
    for j = 1, height do
      table.insert(prev[i], r() and {a = 1, b = 0} or {a = 0, b = 1})
      table.insert(nxt[i], {a = 1, b = 0})
    end
  end

  for i = 1, width do
    for j = 1, height do
      if i > 50 and i < 80
        and j > 30 and j < 50 then
      else
      prev[i][j].a = 1
      prev[i][j].b = 0
      end
  end
  end

  function laplace(at, tbl, x, y)
    local out = 0
    out = out + tbl[x][y][at] * -1

    out = out + tbl[x-1][y][at] * .2
    out = out + tbl[x+1][y][at] * .2
    out = out + tbl[x][y-1][at] * .2
    out = out + tbl[x][y+1][at] * .2

    out = out + tbl[x-1][y+1][at] * .05
    out = out + tbl[x+1][y+1][at] * .05
    out = out + tbl[x-1][y-1][at] * .05
    out = out + tbl[x+1][y-1][at] * .05

    return out
  end

  function cc.update(dt)
  -- calc
  dt = dt *10
  for i = 2, width-1 do
    for j = 2, height-1 do
      local pa = prev[i][j].a
      local pb = prev[i][j].b
      local na = pa +
                 dt *
                 (da * laplace("a", prev, i, j)
                   - pa * pb * pb
                   + feed * (1 - pa))
      local nb = pb +
                 dt *
                 (db * laplace("b", prev, i, j)
                   + pa * pb * pb
                   -(kill + feed) * pb)

        nxt[i][j].a = na
        nxt[i][j].b = nb
      end
    end
  end

  function cc.draw()
  for i = 2, width-1 do
    for j = 2, height-1 do
      local a, b = nxt[i][j].a, nxt[i][j].b
      if a > b then
        set_color(C_white)
      else
        set_color(C_black)
      end
      -- set_color(mix_color(C_white, C_black, a / b))
      -- set_color(rand_in(bodily))
      gfx.setColor(255, 255 *b, 255 * a, 255)
      gfx.rectangle("fill", (i-1) * scl, (j-1) * scl, scl, scl)
    end
  end

    local t = prev
    prev = nxt
    nxt = t
  end
end

-- shader react diffuse
scenes[0x11] = function ()
  local s = gfx.newShader [[
  extern float dt;
  extern float time;
  extern vec2 res;

  vec2 psize = 1./res;

  extern float dA = 1.0;
  extern float dB = 0.4;
  extern float feed = 0.013;
  extern float kill = 0.047;


  /*
  float dA = 1.0;
  float dB = 0.4;
  float feed = 0.013;
  float kill = 0.047;
  */

  float tri(float val) {
       float i;
       val = modf(abs(val), i);
       if (mod(i, 2) == 0)
         val = (1-val);
       return val;
  }

  vec4 getcolor(Image tx, vec2 pos, vec2 offset) {
    return texture2D(tx, pos + (offset * psize));
  }

  vec4 laplace(Image tex, vec2 pos) {
    vec4 ret = vec4(0.);
    ret += -1 * getcolor(tex, pos, vec2(0., 0.));

    ret += 0.2 * getcolor(tex, pos, vec2(1., 0.));
    ret += 0.2 * getcolor(tex, pos, vec2(-1., 0.));
    ret += 0.2 * getcolor(tex, pos, vec2(0., 1.));
    ret += 0.2 * getcolor(tex, pos, vec2(0., -1.));

    // 8
    // 3
    // 3
    // 8
    ret += 0.05 * getcolor(tex, pos, vec2(1., -1.));
    ret += 0.05 * getcolor(tex, pos, vec2(1., 1.));
    ret += 0.05 * getcolor(tex, pos, vec2(-1., -1.));
    ret += 0.05 * getcolor(tex, pos, vec2(-1., 1.));

    return ret;
  }

  void effects(vec4 _color, Image tex, vec2 txc, vec2 scc) {
    // vec4 curr = texture2D(tex, txc - (0.001)) /2;
    // vec4 curr = texture2D(tex, txc+vec2(sin(time)/4000,cos(time/3)/3000));
    vec4 curr = texture2D(tex, txc) ;

    // float a = curr.r-sin(time)/800+0.002;
    float a = curr.r;
    // float a = curr.r-sin(time)/400+0.001;
    float b = curr.g;

    float t = dt;
    t *= 60;
    t=1;

    // kill *= txc.x + .5;
    // feed *= txc.y * 2;

    a += t * (dA * laplace(tex, txc).r
              - a * b * b
              + feed * (1 - a));

    b += t * (dB * laplace(tex, txc).g
              + a * b * b
              - (kill + feed) * b);

    a = clamp(a, 0, 1);
    b = clamp(b, 0, 1);

    love_Canvases[0] = vec4(a, b, 0, 1);

    float col = a - b;
    // col = sin(col+time) /2 + 0.5;
    // col = smoothstep(0.1, 0.3, col);
    float blu = abs(col-0.5);
    float red = abs(col-0.3);
    love_Canvases[1] = vec4(red, red * blu, blu, 1);

    /*
    float col = a - b;
    col = smoothstep(0.2, 0.8, col);
    love_Canvases[1] = vec4(col, col, col, 1);
    */
  }
  ]]

  shader_safe_set(s, "res", { 800, 600 })

  shader_safe_set(s, "dA", 1.0)
  shader_safe_set(s, "dB", 0.6)
  shader_safe_set(s, "feed", 0.027)
  shader_safe_set(s, "kill", 0.053)

  shader_safe_set(s, "dA", 0.9)
  shader_safe_set(s, "dB", 0.4)
  shader_safe_set(s, "feed", 0.077)
  shader_safe_set(s, "kill", 0.062)

  --[[
  shader_safe_set(s, "dA", 0.2)
  shader_safe_set(s, "dB", 1.3)
  shader_safe_set(s, "feed", 0.027)
  shader_safe_set(s, "kill", 0.092)
  --]]

  -- might have to grab record canvas

  local last = gfx.newCanvas()
  local curr = gfx.newCanvas()
  local draw = gfx.newCanvas()

  gfx.setCanvas(last)
    gfx.clear()

    gfx.setColor(255, 0, 0, 255)
    gfx.rectangle("fill", 0, 0, 800, 600)

    -- gfx.setColor(255, 55, 0, 255)
    -- gfx.draw(gfx.newImage("content/yellow.png"), 0,0,0,1.5)

    -- gfx.print("hanta", 50, 50, 0, 5)
    local a = 300
    -- gfx.setColor(0, 255, 0, 255)
    -- gfx.rectangle("fill", a, a, 50, 50)

    -- gfx.setColor(0, 255, 0, 255)
    -- gfx.circle("fill", a - 30, a - 30, 50)
  gfx.setCanvas()

  local t = 0
  function cc.update(dt)
    t = t + dt
    shader_safe_set(s, "time", t)
    shader_safe_set(s, "dt", dt)
  end

  function cc.draw()
    set_color(C_white)

    love.graphics.setBlendMode("alpha", "premultiplied")

    gfx.setCanvas(curr, draw)
      gfx.setShader(s)
      gfx.draw(last)

      if love.mouse.isDown(1) then
        local x, y = love.mouse.getPosition()
        -- gfx.setColor(0, 255, 0, 255)
        gfx.circle("fill", x, y, rand(5,15))
      end
    gfx.setCanvas()

    gfx.setShader() -- set to color shader

    -- gfx.draw(draw, -500, -500, 0, 3, 3)
    gfx.draw(draw)

    local temp = last
    last = curr
    curr = temp
  -- gfx.setColor(255, 0, 0, 255)
  -- gfx.rectangle("fill", 10, 10, 20, 20)
  end
end
