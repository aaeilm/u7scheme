#| xxx
(define prog (make-program (make-vert-string)
  (make-frag-string
  "
  uniform float time;

  void effect() {
    float x = snoise(
      vec3(vert_uv.x * 3 + sin(vert_uv.y + time/2)
         , vert_uv.y * 3 + sin(vert_uv.x*10+time)/6
         , time))/2 + 0.5;
    float c = snoise(
      vec3(vert_uv.x * 3 + sin(vert_uv.y + time/2)
         , vert_uv.y * 3 + sin(vert_uv.x*7+time)/6 + sin(vert_uv.x*3-time)/2
         , time + 0.2))/2 + 0.5;
    c = smoothstep(0.2, 0.8, c);
    vec3 o = mix(vec3(0.4, 0.8, 0.3), vec3(0.4, 0.1, 0.2), x);
    vec3 r = mix(vec3(0.9, 1.0, 0.3), vec3(0.4, 0.8, 0.1), c);
    vec3 y = abs(r-o);
    // vec3 y = r*(1-o) + (1-r)*o;
    color = vec4(y.rgb,1);
  }
  ")))
|#

(change-directory "/home/mel/desktop/portfolio video/prog")

;; todo use macro for for?

;#|
(use
  (srfi 1 133))

; todo get rid of auto call fns (just return the thing
(define (make-proto . args)
  (let (
    (ht (make-hash-table))
    )
    (map
      (lambda (a)
        (hash-table-set! ht (car a) (cdr a)))
      args)
    (lambda (message . args)
      (case message
        ((_set!)
          (hash-table-set! ht (car args) (cadr args)))
        ((_get)
          (hash-table-ref/default ht (car args) #f))
        ((_ht)
          ht)
        ((_pairs)
          (hash-table->alist ht))
        (else
          (let (
            (val (hash-table-ref/default ht message #f))
            )
            (if (procedure? val)
                (apply val args)
                val)))
        ))
    ))

(define := '_set!)

(define (make-framelimiter ct)
  (let (
    (acc 0)
    )
    (lambda ()
      (set! acc (+ acc 1))
      (if (>= acc ct)
          (begin
            (set! acc 0)
            #t)
          #f))))

(define (make-rpg #!optional prob)
  (let (
    (prob (or prob 0.5))
    )
    (lambda ()
      (> prob (rand)))))

(define (make-rng . args)
  (if (every exact? args)
      (lambda ()
        (apply rand args))
      (lambda ()
        (let (
          (r (rand))
          (min (car args))
          (max (cadr args))
          )
          (*+ r (- max min) min)))
      ))

(define (rng seed)
  (lambda (range)
    (randomize seed)
    (set! seed (random rand-max))
    (random range)))

(define (len of)
  (cond
    ((vector? of)
      (vector-length of))
    ((pair? of)
      (length of))
    ((string? of)
      (string-length of))
    ((queue? of)
      (length (queue.as-list of)))
    (else
      (error "cannot take len"))
    ))

(define (make-vec ct fn)
  (let (
    (v (make-vector ct))
    )
    (for 0 ct 1
      (lambda (i)
        (vector-set! v i (fn i))))
    v))

(define (make-point x y)
  (let (
    (ret (make-proto))
    )
    (ret := 'x x)
    (ret := 'y y)
    ret))

(define (ssub str i j)
  (let (
    (j (min j (string-length str)))
    )
    (substring str i j)))

(define (make-Color r g b #!optional a)
  (make-proto `(r . ,r) `(g . ,g) `(b . ,b) `(a . ,(or a 1))))

#|
(define (make-color r g b #!optional a)
  (let (
    (v (make-f32vector 4))
    )
    (f32vector-set! v 0 r)
    (f32vector-set! v 1 g)
    (f32vector-set! v 2 b)
    (f32vector-set! v 3 (or a 1))
    (lambda (msg)
      (f32vector-ref v
        (case msg
          ((r) 0)
          ((g) 1)
          ((b) 2)
          ((a) 3))))
    ))
|#

(define (set-Color! to)
  (set-color! (to 'r) (to 'g) (to 'b) (to 'a)))

(define (mix-Color a b #!optional amt)
  (let* (
    (amt (or amt 0.5))
    (inv (- 1 amt))
    )
    (make-Color
      (+ (* (a 'r) inv) (* (b 'r) amt))
      (+ (* (a 'g) inv) (* (b 'g) amt))
      (+ (* (a 'b) inv) (* (b 'b) amt))
      (+ (* (a 'a) inv) (* (b 'a) amt))
      )))

(define (make-Colors vec)
  (vector-map
    (lambda (c)
      (apply make-Color
        (map
          (lambda (x)
            (/ x 255))
          c))
      )
    vec))

(define good-colors
  (make-Colors '#(
  ( 180 180  10)
  (  22  22  22)
  (  44  44  44)
  (  30 180 120)
  ( 180  40  40)
  (  80  60 180)
  )))

(define bodily
  (make-Colors '#(
  ( 240  30   0)
  ( 150   0  80)
  ( 230  20  20)
  (  68  32  20)
  (  15  15  15)
  ; (  81  25   8)
  ; ( 200   0   0)
  )))

(define diatoms
  (make-Colors '#(
  ( 160 160 170)
  ( 125 125 125)
  (  80 145  20)
  ( 120 145  20)
  )))

(define redblack
  (make-Colors '#(
  ( 200  45   0)
  (  10  10  10)
  (   5   5   5)
  )))

(define mold
  (make-Colors '#(
  ( 125  60 125)
  (  60 200 125)
  (  60 125  60)
  ( 125 125 125)
  )))

(define (load-bacterias)
  (let (
    (loc "content/bacterias")
    )
    (apply vector
      (map
        (lambda (x)
          (let* (
            (file (make-pathname loc x))
            (img (make-image file))
            (tex (make-texture img))
            )
            tex))
        (directory loc)))
    ))

(load-once bacterias
  (load-bacterias))

;|#

; ; ; ; ; ;

(define-struct queue
  (mut head)
  (mut tail)
  )

(define-record-printer (queue obj port)
  (fprintf port
    "#queue(~a . ~a)"
    (queue.head obj)
    (queue.tail obj)
    ))

(define (make-queue)
  (_make-queue '() '()))

(define (make-q ct fn)
  (let (
    (ret (make-queue))
    )
    (for 0 ct 1
      (lambda (i)
        (queue.push! ret (fn i))))
    ret))

(define (queue.empty? q)
  (null? (queue.head q)))

(define (queue.push! q obj)
  (let (
    (ptr (list obj))
    )
    (if (queue.empty? q)
        (begin
          (queue.head! q ptr)
          (queue.tail! q ptr))
        (begin
          (set-cdr! (queue.tail q) ptr)
          (queue.tail! q ptr)))
    ))

(define (queue.pop! q)
  (if (queue.empty? q)
      #f
      (let (
        (ret (car (queue.head q)))
        )
        (queue.head! q (cdr (queue.head q)))
        (when (null? (queue.head q))
          (queue.tail! q '()))
        ret)))

(define queue.as-list queue.head)

(define-struct anim
  (immut frames)
  (mut at))

(define (make-anim f)
  (_make-anim f 0))

(define (anim.peek a)
  (vector-ref (anim.frames a) (anim.at a)))

(define (anim.next! a)
  (anim.at! a (+ (anim.at a) 1))
  (when (>= (anim.at a) (vector-length (anim.frames a)))
    (anim.at! a 0))
  (anim.peek a))

(define (anim.prev! a)
  (anim.at! a (- (anim.at a) 1))
  (when (< (anim.at a) 0)
    (anim.at! a (- (vector-length (anim.frames a)) 1)))
  (anim.peek a))

(define (anim.reset! a #!optional at)
  (let* (
    (at (or at 0))
    (at (modulo at (vector-length (anim.frames a))))
    )
    (anim.at! a at)))

(define (load-video directory from to)
  (let (
    (ret
      (make-vec (- to from)
        (lambda (i)
          (let (
            (% (inexact->exact (floor (* (/ i (- to from)) 100))))
            )
            (if (= (modulo i 2) 0)
              (display (sprintf "\033[2K\rloadin' ~a%" %)))
            (flush-output)
            (make-texture
              (make-image
                (sprintf "~a/~a.png"
                  directory
                  (string-pad (number->string (+ i from)) 6 #\0))))
            ))))
    )
    (display "\033[2K\rloaded 100%")
    (newline)
    ret))

(define (list-skip lst every #!optional offset)
  (let (
    (offset (or offset 0))
    )
    (let loop (
      (lst lst)
      (acc '())
      (n 0)
      )
      (if (null? lst)
          (reverse acc)
          (if (= (modulo (+ n offset) every) 0)
              (loop (cdr lst) (cons (car lst) acc) (add1 n))
              (loop (cdr lst) acc (add1 n)))
    ))))

;;

; todo do mesh in CPP someday
(define-struct mesh
  (immut vao)
  (immut vbo)
  (immut ebo)
  (immut verts)
  (immut indcs)
  (mut draw-wireframe)
  )

; verts are x y u v
; mesh is 2D
(define (make-mesh verts indcs draw-type)
  (let (
    (vao (make-vao))
    (vbo (make-buffer))
    (ebo (make-buffer))
    )
    (gl:bind-vertex-array vao)
    (gl:bind-buffer gl:+array-buffer+ vbo)
    (gl:bind-buffer gl:+element-array-buffer+ ebo)

    (gl:buffer-data gl:+array-buffer+ (* 4 (f32vector-length verts))
      (make-locative verts) draw-type)
    (gl:buffer-data gl:+element-array-buffer+ (* 4 (u32vector-length indcs))
      (make-locative indcs) gl:+static-draw+)

    (gl:vertex-attrib-pointer 0 2 gl:+float+ #f (* 4 4) (address->pointer 0))
    (gl:vertex-attrib-pointer 1 2 gl:+float+ #f (* 4 4) (address->pointer (* 2 4)))
    (gl:enable-vertex-attrib-array 0)
    (gl:enable-vertex-attrib-array 1)

    (gl:bind-vertex-array 0)

    (_make-mesh vao vbo ebo verts indcs #f)))

(define (load-obj-file filename draw-type)
  (let* (
    (lines (read-lines filename))
    (cts
      (fold-right
        (lambda (ln acc)
          (cond
            ((char=? (string-ref ln 0) #\v)
              (if (char=? (string-ref ln 1) #\t)
                  (cons* (car acc) (add1 (cadr acc)) (cddr acc))
                  (cons* (add1 (car acc)) (cadr acc) (cddr acc))
                  ))
            ((char=? (string-ref ln 0) #\f)
              (cons* (car acc) (cadr acc) (add1 (cddr acc))))
            (else
              acc)))
        '(0 0 . 0)
        lines))
    (v-ct (car cts))
    (vt-ct (cadr cts))
    (f-ct (cddr cts))
    (vts (make-vector vt-ct))
    (v-out (make-f32vector (* 4 v-ct)))
    (i-out (make-u32vector (* 3 f-ct)))
    (vs-at 0)
    (vts-at 0)
    (indc-at 0)
    )
    (for-each
      (lambda (ln)
        (cond
          ((char=? (string-ref ln 0) #\o)
            (cons 'o
              (substring ln 2 (string-length ln))))
          ((char=? (string-ref ln 0) #\v)
            (let (
              (val (map string->number
                     (irregex-extract 'real ln)))
              )
              (if (char=? (string-ref ln 1) #\t)
                  (begin
                    (vector-set! vts vts-at val)
                    (+=! vts-at 1))
                  (begin
                    (f32vector-set! v-out (*+ vs-at 4 0) (car val))
                    (f32vector-set! v-out (*+ vs-at 4 1) (- (cadr val)))
                    (+=! vs-at 1)))
              ))
          ((char=? (string-ref ln 0) #\f)
            (let (
              (st (map string->number (irregex-extract 'integer ln)))
              )
              (u32vector-set! i-out (*+ indc-at 3 0) (- (list-ref st 0) 1))
              (u32vector-set! i-out (*+ indc-at 3 1) (- (list-ref st 2) 1))
              (u32vector-set! i-out (*+ indc-at 3 2) (- (list-ref st 4) 1))
              (+=! indc-at 1)
              (let loop (
                (lst st)
                )
                (unless (null? lst)
                  (let* (
                    (v   (- (car lst) 1))
                    (vt  (- (cadr lst) 1))
                    (vt* (vector-ref vts vt))
                    )
                    (f32vector-set! v-out (*+ v 4 2) (car vt*))
                    (f32vector-set! v-out (*+ v 4 3) (- 1 (cadr vt*)))
                    )
                  (loop (cddr lst)))
                )
              ))
            ))
        lines)
    (make-mesh v-out i-out draw-type)
    ))

(define (mesh.sub! mesh pt-at x y uvx uvy)
  (let* (
    (vs (mesh.verts mesh))
    (maybe-set!
      (lambda (offset to)
        (when to
          (f32vector-set! vs (*+ pt-at 4 offset) to))))
    )
    (maybe-set! 0 x)
    (maybe-set! 1 y)
    (maybe-set! 2 uvx)
    (maybe-set! 3 uvy)
    ))

(define (mesh.push-changes! mesh min max)
  (gl:bind-buffer gl:+array-buffer+ (mesh.vbo mesh))
  (gl:buffer-sub-data gl:+array-buffer+  (* 4 4 min) (* 4 4 (- max min))
    (make-locative (mesh.verts mesh)))
  )

; (mesh.sub! mesh pt-at num-or-false nf nf nf)
; (mesh.push-changes! mesh rangemin range-max)

(define m (load-obj-file "content/cutouttest.obj" gl:+stream-draw+))
(define tx (make-texture (make-image "content/pg8 042.png")))
(gl:bind-texture gl:+texture-2d+ tx)
(texture-set-wrap gl:+texture-2d+ gl:+clamp-to-border+ gl:+clamp-to-border+)
(texture-set-border-color gl:+texture-2d+ 0 0 0 0)
(gl:bind-texture gl:+texture-2d+ 0)

(mesh.sub! m 0 -5 10 #f #f)
(mesh.sub! m 1 -4 10 #f #f)
(mesh.push-changes! m 0 2)

(define p
  (make-program
    (make-frag-string
      "
      vec4 effect() {
         return vec4(uv_coord.x, uv_coord.y, 0.0, 1.0);
        // return vec4(1,1,1,1);
      }
      "
      )))

(define (mesh)
  (let (
    )
    (lambda (dt f-at)
      (define len (u32vector-length (mesh.indcs m)))
      (rect! 10 10 10 10)
      (set-shader! default-shader)
      (set-color! 1 1 1)
      (set-transform-uniforms! 180 180 0 20 20)
      (gl:bind-vertex-array (mesh.vao m))

      #|
      (gl:polygon-mode gl:+front-and-back+ gl:+line+)
      (gl:active-texture gl:+texture0+)
      (gl:bind-texture gl:+texture-2d+ default-texture)
      (gl:draw-elements gl:+triangles+ len gl:+unsigned-int+ (address->pointer 0))
      |#

      (gl:polygon-mode gl:+front-and-back+ gl:+fill+)
      (gl:active-texture gl:+texture0+)
      (gl:bind-texture gl:+texture-2d+ tx)
      (gl:draw-elements gl:+triangles+
        (u32vector-length (mesh.indcs m))
        gl:+unsigned-int+
        (address->pointer 0))

      (draw! tx)

      (for 0 (f32vector-length (mesh.verts m)) 4
        (lambda (i)
          (let (
            (x (*+ (f32vector-ref (mesh.verts m) (+ i 0)) 20 180))
            (y (*+ (f32vector-ref (mesh.verts m) (+ i 1)) 20 180))
            )
            (print! (number->string (/ i 4)) x y)
            )
          ))
      0)))

(define g (mesh))
(define (frame)
  (g 0 0))

)

(define (graff)
  (let (
    )
    (lambda (dt f-at)
      0)))

(define g (graff))
(define (frame dt)
  (g 0 0))







(define fn2
  (make-image
    "content/numbers_font.png"))

(define fnt2
                ; 01122234445556666777777889
  (make-font fn2 "01c2ef34ij5lm6opq7stuvw8y9"))

(define (frame dt)
  (set-font! fnt2)
  (print! "012344568789" 0 0 0 2 2))


(define (grad)
  (let* (
    (prog
      (make-program
        (make-frag-string
        "
        uniform float time;
        vec4 effect() {
          float x = uv_coord.y;
          float y = uv_coord.x;
          float flicker = step(abs(snoise(vec2(time/2, 0))), 0.7);
          flicker = (1 - flicker ) / 2;
          vec3 c1 = hcl(vec3(x / 16 +0.5, 0.9 * y + cos(time/2)/5 +flicker - 0.2, 0.4));
          return vec4(c1, 1);
        }
        ")
        ))
    (t 0)
    (loc (safe-get-uniform-location prog "time"))
    )
    (lambda (dt f-at)
      (+=! t dt)
      (set-shader! prog)
      (gl:uniform1f loc t)
      (draw-px! default-texture)
      )))

(define g (grad))
(define (frame dt)
  (g 0.04 0))

(define (bar-circs)
  (let* (
    (F (make-framelimiter 3))
    (r 120)
    (C good-colors)
    (ct (/+ (get-radius) r 2))
    (circs (make-q ct
             (lambda (i)
               (rand-in C))))
    (cc (get-center))
    (cx (car cc))
    (cy (cdr cc))
    (fb (make-framebuffer window-width window-height))
    (tx
      (let (
        (vec (make-s32vector 1))
        )
        (gl:bind-framebuffer gl:+framebuffer+ fb)
        (gl:get-framebuffer-attachment-parameteriv gl:+framebuffer+ gl:+color-attachment0+
          gl:+framebuffer-attachment-object-name+ vec)
        (s32vector-ref vec 0)))
    (prog
      (make-program
        (make-frag-string
        "
  float s_kernel[9] = float[](
    -1, -1, -1,
    -1,  10, -1,
    -1, -1, -1
  );
  float s_avg = 1;

  float bk[9] = float[](
    6,3,6,
    3,0,3,
    6,3,6
  );
  float ba = 1.0/36.0;

  vec4 kernel3x3(float amt, float kernel[9], float avg) {
    vec2 offsets[9] = vec2[](
      vec2( -amt,  amt),
      vec2( 0.0f,  amt),
      vec2(  amt,  amt),
      vec2( -amt, 0.0f),
      vec2( 0.0f, 0.0f),
      vec2(  amt, 0.0f),
      vec2( -amt, -amt),
      vec2( 0.0f, -amt),
      vec2(  amt, -amt)
    );

    vec4 sample[9];
    for(int i = 0; i < 9; ++i) {
        sample[i] = texture2D(tex, uv_coord + offsets[i]);
    }

    vec4 col = vec4(0.0);
    for(int i = 0; i < 9; ++i) {
        col += sample[i] * kernel[i] * avg;
    }

    return col;
  }

         uniform float time;
        vec4 effect() {
           // vec4 col = texture2D(tex, vec2(uv_coord.x, uv_coord.y + sin(uv_coord.x + time/2)*5));
            vec4 col = kernel3x3(0.128, s_kernel, s_avg);
            vec4 col2 = kernel3x3(0.008, bk, ba);
            col.r *= 0.7;
            col = abs(col2 - col);
            if (((col.r + col.g + col.b) / 3) < 0.5) {
              col.a = 0;
            }
          // vec4 col = texture2D(tex, vec2(uv_coord.x*abs(fract(time/2)-0.5)*2+0.5, uv_coord.y + uv_coord.x *9));
          // vec4 col = texture2D(tex, uv_coord);
          return col;
          //color = col;
        }
        ")))
    (t 0)
    (tloc (gl:get-uniform-location prog "time"))
    )
    (lambda (dt f-at)
      (when (F)
        (queue.push! circs(rand-in C))
        (queue.pop! circs))
      ; (canvas.bind! cv)
       (gl:bind-framebuffer gl:+framebuffer+ fb)
       (gl:clear gl:+color-buffer-bit+)
       (set-shader!)
      (let loop (
        (lst (queue.as-list circs))
        (n 1)
        )
        (unless (null? lst)
          (set-Color! (car lst))
          (circ! cx cy (* (- ct n) r))
          (loop (cdr lst) (+ n 1))
          ))

      (gl:bind-framebuffer gl:+framebuffer+ 0)

      ; (set-shader! prog)

      ; (gl:uniform1f tloc (+=! t 0.04))
      ; (program.safer-set! "time" (+=! t 0.04))
       (set-color! 1 1 1)
      ; (gfx-draw-canvas! cv)
      (draw! tx)
        )
    ))


(define (wrap x min max)
  (cond
    ((< x min)
      (- max (- min x)))
    ((> x max)
      (+ min (- x max)))
    (else x)))

(define (wind)
  (let* (
    (wh (get-dimensions))
    (w (car wh))
    (h (cdr wh))
    (pt-ct (rand 45 60))
    (wind (rand -8 8))
    (R-small (make-rng 10.0 18.0))
    (R-big (make-rng 20.0 50.0))
    (P-big (make-rpg 0.7))
    (fall (rand 2 4))
    (mult 2)
    (diff 1)
    (pts
      (make-vec pt-ct
        (lambda (i)
          (let (
            (r (make-point (rand w) (rand h)))
            )
            (r := 'r ((if (P-big) R-big R-small)))
            (r := 'w (* (r 'r) diff))
            (r := 'c (mix-Color (rand-in mold) (rand-in bodily) (*+ (rand) 0.5 0.5)))
            (dnL (r 'c '_pairs))
            r))
        ))
    )
    (sort! pts
      (lambda (a b)
        (> (a 'r) (b 'r))))
    (set-shader!)
    (lambda (dt f-at)
      (vector-for-each
        (lambda (p)
          (p := 'x (+ (p 'x) (* (/ 2 (p 'w)) wind mult)))
          (p := 'y (+ (p 'y) (* (/ 2 (p 'w)) 4 fall mult)))
          (p := 'x (wrap (p 'x) (- (p 'r)) (+ w (p 'r))))
          (p := 'y (wrap (p 'y) (- (p 'r)) (+ h (p 'r))))
          (set-Color! (p '_get 'c))
          (circ! (p 'x) (p 'y) (p 'r)))
        pts))
    ))

(define w (wind))
(define (frame f-at)
  (w 0 0))

(define (dna-plain)
  (let* (
    (wh (gfx-get-dimensions))
    (w (car wh))
    (h (cdr wh))

    (ymin -40)
    (ymax (+ h 40))
    (yrange (- ymax ymin))

    (bar-ct 25)
    (bar-spc (/ yrange bar-ct))

    (colora (make-color 0 0 1))
    (colorb (make-color 1 0 1))

    (v-spd 10)

    (bars (make-q bar-ct
            (lambda (i)
              (cons (coinflip)
                    (+ (* i bar-spc) ymin))
              )))
    )
    (lambda (dt f-at)
      (map
        (lambda (bar)
          (set-cdr! bar (- (cdr bar) v-spd)))
        (queue.as-list bars))
      (when (<= (cdar (queue.head bars)) ymin)
        (let (
          (it (queue.pop! bars))
          )
          (set-car! it (coinflip))
          (set-cdr! it (- ymax (- ymin (cdr it))))
          (queue.push! bars it)))
      (map
        (lambda (bar)
          (if (car bar)
            (set-color! colora)
            (set-color! colorb))
          (gfx-rect! 30 (cdr bar) 30 8))
        (queue.as-list bars))
    )))

(define (dna-spin)
  (let* (
    (wh (get-dimensions))
    (w (car wh))
    (h (cdr wh))

    (ymin -40)
    (ymax (+ h 40))
    (yrange (- ymax ymin))

    (bar-ct 15)
    (bar-spc (/ yrange bar-ct))

    (width 100)
    (r-spd 0.1)
    (phs-offset (* pi 0.55))
    (strs #("at" "gc"))
    (v-spd 4)
    (turns 2)

    (bars (make-q bar-ct
            (lambda (i)
              (cons (rand-in strs)
                    (+ (* i bar-spc) ymin))
              )))

    (draw-bar
      (lambda (bar phs)
        (let* (
          (x1 (sin phs))
          (x2 (sin (+ phs phs-offset)))
          (over (> x2 x1))
          (x1 (*+ x1 width (+ 20 width)))
          (x2 (*+ x2 width (+ 20 width)))
          (y (cdr bar))
          )
          ; (gfx-circ! x1 (cdr bar) 5)
          ; (gfx-circ! x2 (cdr bar) 5)
          (set-color! 0.2 0.9 0.1)
          (rect! (if over x1 x2) y (abs (- x2 x1)) 40)
          #|
          (if over
            (begin
            (gfx-set-color! 0 0 0)
            (gfx-rect! x1 y 12 20 'fill)
            (gfx-set-color! 1 0 0)
            (gfx-rect! x1 y 12 20 'line)
            (gfx-rect! x2 y 12 20 'fill)
            (gfx-rect! x2 y 12 20 'line)
            )
            (begin
            (gfx-set-color! 1 0 0)
            (gfx-rect! x2 y 12 20 'fill)
            (gfx-rect! x2 y 12 20 'line)
            (gfx-set-color! 0 0 0)
            (gfx-rect! x1 y 12 20 'fill)
            (gfx-set-color! 1 0 0)
            (gfx-rect! x1 y 12 20 'line)
            ))
            (gfx-set-color! 1 0 0)
          (gfx-line! x1 y x2 y)
          (gfx-line! x1 (+ 20 y) x2 (+ 20 y))
          |#
          )))

    (g-phs 0)

    )
    (lambda (dt f-at)
      (+=! g-phs r-spd)
      (map
        (lambda (bar)
          (set-cdr! bar (- (cdr bar) v-spd))
          (let* (
            (ratio (* pi turns (/ (- (cdr bar) ymin) yrange)))
            )
            (draw-bar bar (+ ratio g-phs))
          ; (gfx-print! (car bar) 60 (cdr bar) 0 2 2)
          ; (gfx-rect! 30 (cdr bar) 30 8)
          ))
        (queue.as-list bars))
      (when (<= (cdar (queue.head bars)) ymin)
        (let (
          (it (queue.pop! bars))
          )
          (set-car! it (rand-in strs))
          (set-cdr! it (- ymax (- ymin (cdr it))))
          (queue.push! bars it)
          ))
    )))

(define x (dna-spin))
(define (frame dt)
  (x dt 0))

#|
(set-tempo! 170)
(set-audio! "u7 dratama rsz.wav")
(loop! (beats 8) (beats 16))
|#

)


(define (bar-chart w spc csz)
  (let* (
    (F (make-framelimiter 5))
    (P-move (make-rpg 0.4))
    (R-jump (make-rng -34 34))
    (min 10)
    (max 500)
    (ct (inexact->exact (floor (/ 800 (+ w spc)))))
    (heights (make-vec ct
               (lambda (i)
                 (rand min max))))
    )
    (lambda (dt x)
      (when (F)
        (vector-map!
          (lambda (v)
            (if (P-move)
                (clamp (+ v (R-jump)) min max)
                v))
          heights))

      (gfx-set-color! 1 1 1)
      (for 0 (- (len heights) 1) 1
        (lambda (i)
          (let (
            (y (vector-ref heights i))
            (x (+ spc (* i (+ w spc))))
            )
            (gfx-rect! x 0 w y)
            )
          ))
      )
    ))

(define (rays params)
  (let* (
    (spd (or (params 'spd) 0.004))
    (ct (or (params 'ct) 6))
    (total-ct (* ct 2))
    (phs 0)
    (offset (/ (* pi 2) total-ct))
    (sz 800)
    )
    (lambda (dt x)
      (+=! phs spd)
      (for 0 total-ct 1
        (lambda (n)
          (let* (
            (ox 400)
            (oy 300)
            (one (to-cartesian sz (+ phs (* n offset))))
            (two (to-cartesian sz (+ phs (* (+ n 1) offset))))
            (x1 (+ ox (car one)))
            (y1 (+ oy (cdr one)))
            (x2 (+ ox (car two)))
            (y2 (+ oy (cdr two)))
            )
            (if (= (modulo n 2) 0)
                (gfx-set-color! 1 1 1)
                (gfx-set-color! 0 0 0))
            (gfx-tri! 400 300 x1 y1 x2 y2)
            ))
        )
      )
  ))

(define (color-bars)
  (let* (
    (nspd 0.01)
    (ndiff 0.1)
    (min 15)
    (max 80)
    (R-width (make-rng 0.25 1.05))
    (C bodily)
    (ct 100)
    (colors (make-vec ct
              (lambda (i)
                (rand-in C)
                )))
    (w-mult (make-vec ct
              (lambda (i)
                (R-width)
                )))
    (range (- max min))
    (noisex (rand 0 100))
    )
    (lambda (dt f-at)
      (+=! noisex nspd)
      (let (
        (x 0)
        )
        (for 0 ct 1
          (lambda (n)
            (let* (
              (idx (+ (* ndiff n) noisex))
              (w (* (vector-ref w-mult n)
                    (*+ (/+ (perlin idx 0) 2 0.5) range min)))
              )
              ; (gfx-set-color! (rand) (rand) (rand))
              (set-color! (vector-ref colors n))
              (gfx-rect! x 0 w 600)
              (+=! x w)
              )))
        ))
  ))

; (define x (color-bars))

#|
(define (frame f-at)
  (gfx-rect! 0 0 10 10)
  (x 0 f-at))
|#

(define C-white
  (make-color 1 1 1))

(define (protozoa args)
  (let* (
    (prob (or (args 'prob) 0.02))
    (xr 0.004)
    (yr 0.004)
    (xs 10)
    (ys 10)
    (R-spin-spd (make-rng 3.0 9.0))
    (R-mov (make-rng 1.0 15.0))
    (R-mov (lambda () 0))
    (P-reverse (make-rpg 0.5))
    (C bodily)
    (pts
      (let* (
        (wh (gfx-get-dimensions))
        (w (car wh))
        (h (cdr wh))
        (nx (rand 1 100))
        (ny (rand 1 100))
        (collect '())
        )
        (for 0 (/ w xs) 1
          (lambda (xi)
            (for 0 (/ h ys) 1
              (lambda (yi)
                (when (< (rand)
                         (* prob (/+ (perlin (+ nx (* xi xr))
                                             (+ ny (* yi yr)))
                                 2 .5))
                      )
                  (let (
                    (pt (make-point (* xi xs)
                                    (* yi ys)))
                    )
                    (pt := 'r (* (rand 0 3) 90))
                    (pt := 'img (rand-in bacterias))
                    (pt := 'phs (rand 0 360))
                    (pt := 'rate ((if (P-reverse) - +) (R-spin-spd)))
                    (pt := 'mov (R-mov))
                    (pt := 'turn (rand 80 110))
                    (pt := 'c (mix-color (rand-in C) C-white))
                  (set! collect (cons pt collect))))
                  ))))
          (list->vector collect)))
    )
    ; shuffle
    (for 0 (/ (len pts) 2) 1
      (lambda (i)
        (let (
          (tmp (vector-ref pts i))
          (at (rand (len pts)))
          )
          (vector-set! pts i (vector-ref pts at))
          (vector-set! pts at tmp))))
    (lambda (dt f-at)
      (gfx-set-color! 1 1 1)
      (vector-map
        (lambda (pt)
          (let* (
            (xy (to-cartesian (pt 'mov) (rads (pt 'phs))))
            (x (+ (pt 'x) (car xy)))
            (y (+ (pt 'y) (cdr xy)))
            )
            (pt := 'phs (+ (pt 'phs) (pt 'rate)))
            ; xxx this is slow lol
            (set-color! (pt '_get 'c))
            (gfx-draw! (pt 'img) x y
              (rads (+ (pt 'r) (pt 'phs) (pt 'turn))) 1.8)
            ))
        pts)
      )
    ))

#|
(define px (protozoa (make-proto `(prob . 0.02))))

(define (frame f-at)
  (px 0 0))
|#

(define (chemicals)
  (let* (
    (F (make-framelimiter 1))
    (R-str-len (make-rng 3 9))
    (chain-ct 13)
    (v-spc 24)
    (hang 5)
    (hard-max 18)
    (base-x 60)

    (make-glyph
      (lambda (str #!optional offset must-have-next)
        (make-proto
          `(str . ,str)
          `(offset . ,(or offset 0))
          `(must-have-next . ,(or must-have-next #f))
          )))

    (starts "abcj")
    (chems
      (apply make-proto
        (map
          (lambda (g)
            (cons (symbol->string (car g))
                  (apply make-glyph (cdr g))))
         '(
          (a "ccddddegg")
          (b "ppk")
          (c "aaddddeggkk")
          (d "aabbccdjj")
          (e "")
          (g "gggggieeek")
          (i "iiiiigeeek" 0 #t)
          (j "lnmk" 7)
          (k "c" 0 #t)
          (l "acj" 0 #t)
          (m "")
          (n "acccj" 0 #t)
          (p "ccb" 0 #t)
          )
         )))

    (make-chain
      (lambda (str x y)
        (make-proto
          `(x . ,x)
          `(y . ,y)
          `(str . ,str)
          `(at . 0)
          `(go-up . #t))))

    (random-str
      (lambda (curr-char)
        (let (
          (len (R-str-len))
          (str curr-char)
          )
          (let loop (
            (n 1)
            )
            (define nexts (chems curr-char 'str))
            (define have-next (chems curr-char 'must-have-next))
            (unless (or (> n hard-max)
                        (and (not have-next) (> n len))
                        (= (string-length nexts) 0))
              (let (
                (ch (rand-in nexts))
                )
                (..=! str ch)
                (set! curr-char ch))
              (loop (+ n 1))
              )
            )
          str)))

    (rx
      (lambda ()
        (* (rand 0 4) 30)))

    (chains
      (make-vec chain-ct
        (lambda (i)
          (make-chain (random-str (rand-in starts))
                      (rx)
                      (* (- i 1) v-spc))
          )))

    (canv (make-canvas))
    (tx (canvas.tx canv))
    )
    (map
      (lambda (pair)
        (let (
          (str (car pair))
          (gly (cdr pair))
          )
          (gly '_set! 'image
            (make-texture (string-append "content/chems/" str ".png")))))
      (chems '_pairs))
    (texture.set-filter! tx +nearest+ +nearest+)
    (lambda (dt x)
      (gfx-set-color! 0.9 0.9 0.9 1)
      (when (F)
        (vector-map
          (lambda (c)
            (if (c 'go-up)
                (begin
                  (c '_set! 'at (+ (c 'at) 1))
                  (when (>= (c 'at) (+ hang (len (c 'str))))
                    (c '_set! 'go-up #f)))
                (begin
                  (c '_set! 'at (- (c 'at) 1))
                  (when (< (c 'at) 0)
                    (c '_set! 'str (random-str (rand-in starts)))
                    (c '_set! 'x (rx))
                    (c '_set! 'go-up #t)
                    (c '_set! 'at 0)))
              ))
          chains))

      (canvas.bind! canv)
      (gfx-clear!)
      (vector-map
        (lambda (c)
          (let (
            (x (+ (c 'x) base-x))
            (y (c 'y))
            (str (ssub (c 'str) 0 (c 'at)))
            )
            (for 0 (len str) 1
              (lambda (i)
                (let* (
                  (ch (substring str i (+ i 1)))
                  (img (chems ch 'image))
                  (offset (chems ch 'offset))
                  )
                  (gfx-draw! img x y)
                  (+=! x (- (texture.w img) offset 1))
                  )))
            ))
        chains)
      (canvas.unbind! canv)

      ; (gfx-draw-canvas! canv 0 0)
      (gfx-draw! tx -105 1216 0 2 -2)
        )
    ))

(define (bg)
  (let (
    (shd (make-program (make-vert-string)
      (make-frag-string
      "
      uniform float time;

      void effect() {
        float f = snoise(vec3(vert_uv.xy * 2.5, time))/2+0.5;
        // color = vec4(cubehelix(f, sin(time), 0.9, 0.9, cos(time/3)*0.8+1).rgb, 1.0);
        f = 1-abs(f-0.75);
        color = vec4(hcy(vec3(f, mix(vert_uv.x, f, 0.8), 1-f)).rgb, 1.0);
      }
      ")))
    (t 0)
    (acc 0)
    )
    (lambda (dt x)
      (let (
        (dt (miri-get-fps))
        )
      (+=! t dt)
      (gfx-set-shader! shd)
      (program.safe-set! shd uniform1f "time" t)
      (gfx-draw-px! default-texture 0 0 0 800 800)
      (gfx-set-shader!)
      )
    )
    ))

(define (ff b)
  (let (
    (c (chemicals))
    )
    (lambda (dt x)
      (let (
        (dt (miri-get-fps))
        )
      (b dt 0)
      (c dt 0)
      )
    )
    ))

(define (la b)
  (let (
    (bars (bar-chart 40 10 0))
    )
    (lambda (dt x)
      (b dt x)
      (bars dt x)
      )))

(define uo (bg))

;; todo load chemicals once

;;

(miri-set-tempo! 170)
(miri-set-audio! "u7 dratama rsz.wav")

(define clips (make-hash-table))
(define at
  (cut hash-table-set! clips <> <>))

(lambda (make-layer . args)
  ; takes fns and layers them
  0)

(define beats miri-beats)

(at 0 (ff uo))
; (at (beats 2) (color-bars))
(at (beats 3.5) (la uo))
(at (beats 6) (rays (make-proto '(spd . 0.05))))
; (miri-loop! (beats 640) (beats 648))
(miri-loop! (beats 32) (beats 40))

(define dt (miri-get-fps))
(define curr-clip #f)
(define (frame f-at)
  (let (
    (val (hash-table-ref/default clips f-at #f))
    )
    ; (dnL f-at)
    (when val
      (set! curr-clip val))
    (curr-clip dt f-at)
    ))

; (do-export! #f) ; dont (default)
; (do-export! "filename") ; do, to file/folder

(load-once strep-vid
  (load-video "content/videos/strep" 1 407))

(define vid (make-program (make-vert-string)
  (make-frag-string
  "
  uniform float time;

  float s_kernel[9] = float[](
    -1, -1, -1,
    -1,  9, -1,
    -1, -1, -1
  );
  float s_avg = 1;

  float bk[9] = float[](
    0, 4, 10,
    2, 0, 2,
    10, 4, 0
  );
  float ba = 1.0/36.0;

  vec4 kernel3x3(float amt, float kernel[9], float avg) {
    vec2 offsets[9] = vec2[](
      vec2( -amt,  amt),
      vec2( 0.0f,  amt),
      vec2(  amt,  amt),
      vec2( -amt, 0.0f),
      vec2( 0.0f, 0.0f),
      vec2(  amt, 0.0f),
      vec2( -amt, -amt),
      vec2( 0.0f, -amt),
      vec2(  amt, -amt)
    );

    vec4 sample[9];
    for(int i = 0; i < 9; ++i) {
        sample[i] = texture2D(tex, vert_uv + offsets[i]);
    }

    vec4 col = vec4(0.0);
    for(int i = 0; i < 9; ++i) {
        col += sample[i] * kernel[i] * avg;
    }

    return col;
  }

  void effect() {
     vec4 oc = texture2D(tex, vert_uv);
     color =  kernel3x3(fract(time/1)/80, bk, ba);
     // color =  kernel3x3(0.002, s_kernel, s_avg);
    float amt = color.r;
    color = vec4(hcy(vec3(fract(amt + time), 0.5, 0.2)), 1);
    // color = vec4(cubehelix(amt, fract(time/4)*4, 8, 1, 1.5).xyz, 1);
  }
  ")
  ))

(define (aplay at)
  (let* (
    (ax (make-anim strep-vid))
    (last (anim.peek ax))
    (F (make-framelimiter 1))
    (strt 200)
    (t 0)
    (dt (miri-get-fps))
    )
    (anim.reset! ax strt)
    (lambda (f-at)
      (when (= at f-at)
        (anim.reset! ax strt)
        (set! t 0))
      (when (F)
        (set! last (anim.next! ax))
        (anim.next! ax)
        (anim.next! ax))
      (gfx-set-color! 1 1 1)
      (gfx-set-shader! vid)
      (+=! t dt)
      (program.safe-set! vid uniform1f "time" t)
      (gfx-draw-px! last -20 -20 0 840 640)
      (gfx-set-shader!)
      )
      ))

(define l (aplay (beats 32)))
(define pz (protozoa (make-proto)))
(define dn (dna-spin))
(define ch (chemicals))

(define (frame f-at)
  (l f-at)
  (pz dt f-at)
  (ch 0 0)
  ; (dn f-at 0)
  )

#|
(define $ make-params)

; (define fn (bar-chart 40 0 0))
; (define fn (rays ($ '(spd 0.004))))
(define fn (color-bars
))

(define t 0)
(define bact #f)
(define x (make-framelimiter 10))
(set! bact (rand-in bacterias))

(define (frame dt)
  (fn dt 0)
  (+=! t dt)
  (gfx-set-color! 1 1 1)
  (gfx-draw! bact 50 50 t)
  (when (x)
    (set! bact (rand-in bacterias)))
  )
|#

#|
(define tex (make-texture "content/yellow.png"))
(define shd (make-program (make-vert-string)
  (make-frag-string
  "
  uniform float time;

  void effect() {
    float f = snoise(vec3(vert_uv.xy * 2.5, time))/2+0.5;
    // color = vec4(cubehelix(f, sin(time), 0.9, 0.9, cos(time/3)*0.8+1).rgb, 1.0);
    f = 1-abs(f-0.75);
    color = vec4(hcy(vec3(f, mix(vert_uv.x, f, 0.8), 1-f)).rgb, 1.0);
  }
  ")))

(define c (make-canvas))
(define tx (canvas.tx c))

(set! gfx-thickness 5)

(define (frame dt)
  (set! t (+ t dt))
  (gfx-set-shader!)
  (gfx-set-color! 0 0 1)
  (gfx-draw! tex 15)

  (gfx-set-shader! shd)
  (program.safe-set! shd uniform1f "time" t)
  (gfx-draw-px! tex 20 20 0 100 100)

  (canvas.bind! c)
    (gfx-clear!)
    (gfx-set-color! 0 1 1)
    (gfx-print! "yayayayayaya" 10 10 0 5)
  (canvas.unbind! c)

  (gfx-set-shader!)
  (gfx-draw-canvas! c 0 0)

  (gfx-set-color! 1 (/+ (perlin t 0) 2 0.5) 1 1)
  (gfx-set-pt-color! 1 1 1 1 0)
  (gfx-set-pt-color! 2 0 0 1)

  (gfx-tri! 50 50 150 200 10 100)
  (gfx-rect! 50 50 50 50)

  (gfx-set-pt-colors! 1 1 1)
  (gfx-set-color! 0 0 0)
  (gfx-rect! 50 50 50 50 'line)
  )
|#
